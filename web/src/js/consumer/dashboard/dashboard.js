"use strict";

define([
	"app",
	"application/Router",
	"consumer/dashboard/dashboard.controller"
], function (App, AppRouter, Controller) {

	App.module("Consumer.Dashboard", function (Dashboard, App) {
		Dashboard.Router = AppRouter.extend({
			appRoutes: {
				"dashboard": "show",
				"temp-nav": "nav" //TODO: temporary site navigation
			}
		});

		App.addInitializer(function () {
			return new Dashboard.Router({
				controller: Controller
			});
		});

	});

	return App.Consumer.Dashboard;
});
