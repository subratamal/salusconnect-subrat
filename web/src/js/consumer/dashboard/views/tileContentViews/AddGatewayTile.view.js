"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, TileContentViewMixin, SalusView) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {

		Views.AddGatewayTile  = Mn.ItemView.extend({
			className: "add-gateway-tile tile-container",
			template: consumerTemplates["dashboard/tile/addGatewayTile"],
			ui: {
				"title": ".bb-add-new-tile-text"
			},
			events: {
				"click": "_onClick"
			},
			onRender: function () {
				this.ui.title.text(App.translate("dashboard.addGateway"));
			},
			_onClick: function () {
				App.navigate("setup/gateway");
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Dashboard.Views.AddGatewayTile;
});
