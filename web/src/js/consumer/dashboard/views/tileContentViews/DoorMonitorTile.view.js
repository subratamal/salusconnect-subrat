"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/dashboard/views/tileContentViews/mixin.TileContentView",
	"consumer/dashboard/views/tileContentViews/MonitoringTileBack.view"
], function (App, consumerTemplates, TileContentViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {
		Views.DoorMonitorTile = {};

		Views.DoorMonitorTile.FrontView = Mn.ItemView.extend({
			className: "door-tile",
			template: consumerTemplates["dashboard/tile/equipmentTileFront"],
			bindings: {
				":el": { // watch both properties
					observe: ["ErrorIASZSAlarmed1", "ErrorIASZSAlarmed2"],
					onGet: function (alarmProps) {
						var alarm1 = alarmProps[0], alarm2 = alarmProps[1];

						if (alarm1 && alarm2) {
							return alarm1.getProperty() || alarm2.getProperty();
						}

						return false;
					},
					update: function ($el, open) {
						$el.toggleClass("open", !!open);
					}
				}
			},
            initialize: function () {
				this.model.getDevicePropertiesPromise().then (function () {
					
				});

			},
			templateHelpers: function () {
				return {
					isLargeTile: this.isLargeTile
				};
			}
		}).mixin([TileContentViewMixin]);

		Views.DoorMonitorTile.BackView = Views.MonitoringTileBackView;
	});

	return App.Consumer.Dashboard.Views.DoorMonitorTile;
});
