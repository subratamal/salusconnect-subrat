"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Dashboard.Views.TileContentViews", function (Views, App, B, Mn) {
		Views.WelcomeTile = {};

		Views.WelcomeTile.FrontView = Mn.ItemView.extend({
			className: "welcome-tile",
			template: consumerTemplates["dashboard/tile/welcomeTile"],
			events: {
				"click .bb-welcome-tile-dismiss": "dismissWelcomeTile"
			},
			/**
			 * dismisses the welcome tile forever
			 */
			dismissWelcomeTile: function () {
				this.trigger("dismiss");
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Dashboard.Views.WelcomeTile;
});
