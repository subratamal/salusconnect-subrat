"use strict";

/**
 * This model is for all dashboard tiles
 */
define([
	"app"
], function (App) {

	App.module("Consumer.Dashboard.Models", function (Models, App, B) {
		Models.TileModel = B.Model.extend({
			defaults: {
				isLargeTile: true,
				referenceId: null,
				referenceType: "device",
				isFlipped: false
			}
		});
	});

	return App.Consumer.Dashboard.Models.TileModel;
});
