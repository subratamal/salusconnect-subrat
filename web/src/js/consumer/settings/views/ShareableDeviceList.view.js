"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/GatewayForm.view",
	"common/model/ayla/deviceTypes/GatewayAylaDevice",
	"consumer/settings/views/ShareableDeviceListRow.view",
	"consumer/settings/views/EmptyList.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
		Views.ShareableDeviceListView = Mn.CompositeView.extend({
			template: consumerTemplates["settings/manageDevices/shareableDeviceList"],
			emptyView: Views.EmptyListView,
			emptyViewOptions: {
				i18nTextKey: "settings.manageDevices.devices.formLabels.emptyDevices"
			},
			ui: {
				selectAllButton: ".bb-select-all",
				deselectAllButton: ".bb-deselect-all",
			},
			events: {
				"click @ui.selectAllButton": "_selectAllClicked",
				"click @ui.deselectAllButton": "_deselectAllClicked"
			},
			childView: Views.ShareableDeviceListRowView,
			childViewContainer: "#bb-shareable-device-table",
			initialize: function (options) {
				this.childViewOptions = {
					user_id: options.user_id
				};
				this._isCreateMode = options.isCreateMode;
			},
			onRender: function() {
				if (this._isCreateMode) {
					this._selectAllClicked();
				}
			},
			_selectAllClicked: function () {
				this.ui.selectAllButton.addClass("hidden");
				this.ui.deselectAllButton.removeClass("hidden");
				this.$("input").prop("checked", true);
			},
			_deselectAllClicked: function () {
				this.ui.selectAllButton.removeClass("hidden");
				this.ui.deselectAllButton.addClass("hidden");
				this.$("input").prop("checked", false);
			}
		}).mixin([SalusView], {});
	});

	return App.Consumer.Settings.Views.ShareableDeviceListView;
});
