"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/GatewayForm.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.ShareableDeviceListRowView = Mn.ItemView.extend({
			template: consumerTemplates["settings/manageDevices/shareableDeviceListRow"],
			tagName: "li",
			className: "share-table-item col-xs-6",
			ui : {
				"name": ".bb-shareable-device-name",
				"checkbox": ".bb-device-shared-checkbox"
			},
			bindings: {
				".bb-shareable-device-name": {
					observe: "product_name",
					onGet: "getDisplayName"
				},
			},
			initialize: function(options) {
				_.bindAll(this, "getDisplayName");
				this._userId = options.user_id;
				this._shareCollection = App.salusConnector.getShareCollection();
			},
			onRender: function(options) {
				this.ui.name.text(options.model.get("product_name"));
				this.ui.checkbox.prop("checked", this._getCheckState());
			},
			getDisplayName: function () {
				return this.model.getDisplayName();
			},
			_getCheckState: function() {
				var that = this;
				return !!this._shareCollection.findWhere({"user_id": this._userId, "resource_id": that.model.get("dsn")});
			}
		}).mixin([SalusView], {});
	});

	return  App.Consumer.Settings.Views.ShareableDeviceListRowView;
});
