"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/settings/views/GatewayList.view",
	"consumer/settings/views/Shares.view"
], function (App, consumerTemplates, SalusPageMixin, RegisteredRegions) {

	/**
	 * TODO: this page is no longer called ManageDevices... it is called Gateways and should be in the Equipment module
	 * TODO: all manageDevices 'gateway' views and templates should be renamed and moved under equipment packages
	 *
	 * TODO: the shares view will likely be broken off into its own page and stay in Settings module
	 */

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
		Views.ManageDevicesPage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/manageDevices/landing"],
			regions: {
				"gateways": "#bb-settings-gateways",
				"settings": "#bb-settings-devices"
			},
			initialize: function (/*options*/) {
				this.registerRegion("gateways", new Views.GatewayListView());

				if (App.isDevOrInt()) {
					this.registerRegion("settings", new Views.SharesLayout());
				}
			}

		}).mixin([SalusPageMixin, RegisteredRegions], {
			analyticsSection: "settings",
			analyticsPage: "manageDevices"
		});
	});

	return App.Consumer.Settings.Views.ManageDevicesPage;
});