"use strict";

define([
	"app",
	"momentWrapper",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/views/SalusLinkButton.view",
], function (App, moment, config, consumerTemplates, SalusPage, SalusViewMixin,
			 ConsumerMixin, SalusLinkButtonView) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
		Views.AboutAppPage = Mn.LayoutView.extend({
			template: consumerTemplates["settings/aboutThisApp"],
			id: "about-page",
			className: "container",
			ui: {
				"companyName": ".bb-company-name-text",
				"companyUrl": ".bb-company-url-text",
				"buildDateText": ".bb-build-date-text",
				"releasedDateText": ".bb-released-date-text",
				"versionText": ".bb-version-text",
				"copyrightText": ".bb-copyright-text",
				"removeGroupInTileOrder": ".bb-remove-group-in-tile-order button"
			},
			onRender: function () {
				var releaseDate = window.build.releaseDate ? window.build.releaseDate : window.build.buildDate;

				//TODO - check if user is EU or US and set url appropriately
				this.ui.companyUrl.text("www.salusconnect.io"); //http://eu.salusconnect.io/
				this.ui.buildDateText.text(moment(new Date(window.build.buildDate)).utc().format("LL"));
				this.ui.releasedDateText.text(moment(new Date(releaseDate)).utc().format("LL"));
				this.ui.versionText.text(window.build.versionNumber + " " + window.build.buildNumber);

				this.ui.copyrightText.text(App.translate("settings.about.copyrightFormatted", {
					"currentYear": new Date().getFullYear()
				}));
				
				this.ui.removeGroupInTileOrder.click(this.removeGroupInTileOrderClick);
			},
			removeGroupInTileOrderClick: function (){
				App.salusConnector.getSessionUser().get("tileOrderCollection").saveNotGroup();
			}
		}).mixin([SalusPage], {
			analyticsSection: "settings",
			analyticsPage: "about"
		});
	});


	return App.Consumer.Settings.Views.AboutAppPage;
});

