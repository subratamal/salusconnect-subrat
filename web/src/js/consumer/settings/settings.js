"use strict";

define([
	"app",
	"application/Router",
	"consumer/settings/settings.controller"
], function (App, AppRouter, Controller) {

	App.module("Consumer.Settings", function (Settings, App, B, Mn, $, _) {
		Settings.Router = AppRouter.extend({
			appRoutes: {
				"gateways": "gateways",
				"settings/dataCollection": "dataCollection",
				"settings/categories/(:type)": "categorySettings",
				"settings/myEquipment/(:dsn)" : "equipmentSettings",
				"settings/profile": "profileSettings",
				"settings/profile/changePassword": "changePassword",
                "settings/profile/resetPassword": "resetPassword",
				"settings/about": "aboutApp"
			}
		});

		App.addInitializer(function () {
			Settings.PageEventController = {};
			_.extend(Settings.PageEventController, B.Events);
			Settings.router = new Settings.Router({
				controller: Controller
			});
		});
	});
});