"use strict";

define([
	"app",
	"application/Router",
	"consumer/oneTouch/oneTouch.controller",
	"common/model/salusWebServices/rules/RuleMakerManager"
], function (App, AppRouter, Controller) {

	App.module("Consumer.OneTouch", function (OneTouch, App) {
		OneTouch.Router = AppRouter.extend({
			// dont change the order of these routes, it can introduce bus
			appRoutes: {
				"oneTouch": "oneTouch",
                "oneTouch/deviceWindowSensor/(:key)": "deviceWindowSensor",
				"oneTouch/(:dsn)": "oneTouch",
				"oneTouch/(:dsn)/create": "createOneTouch",
				"oneTouch/(:dsn)/apply": "applyOneTouch",
				"oneTouch/(:dsn)/pin": "pinOneTouch",
				"oneTouch/(:dsn)/(:OTKey)": "oneTouchDetail",
				"oneTouch/(:dsn)/(:OTKey)/edit": "editOneTouch"
			}
		});

		App.addInitializer(function () {
			OneTouch.ruleMakerManager = new App.Models.RuleMakerManager();

			return new OneTouch.Router({
				controller: Controller
			});
		});
	});
});