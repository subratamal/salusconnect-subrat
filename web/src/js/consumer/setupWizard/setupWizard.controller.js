"use strict";

define([
	"app",
	"consumer/consumer.controller",
	"consumer/setupWizard/views/SetupLandingPage",
	"consumer/setupWizard/views/SetupGatewayPage",
	"consumer/setupWizard/views/SetupPairingPage.view",
	"consumer/setupWizard/views/provisioning/ProvisioningSelectEquipmentPage.view",
	"consumer/setupWizard/views/provisioning/ProvisioningNameEquipmentPage.view",
	"consumer/setupWizard/views/provisioning/ProvisioningCustomizeEquipmentPage.view"
], function (App, ConsumerController) {

	App.module("Consumer.SetupWizard", function (SetupWizard) {
		SetupWizard.Controller = {
			setupLanding: function setupLanding() {
				var page = new App.Consumer.SetupWizard.Views.SetupLandingPage();

				ConsumerController.showLayout(page, true);
			},
			setupGateway: function setupGateway() {
				var page = new App.Consumer.SetupWizard.Views.SetupGatewayPage();

				ConsumerController.showLayout(page, true);
			},
			setupPairing: function setupPairing() {
				var page = new App.Consumer.SetupWizard.Views.SetupPairingPage();

				ConsumerController.showLayout(page, true, ["devices"]);
			},
			provisionEquipment: function provisionEquipment() {
				var page = new App.Consumer.SetupWizard.Provisioning.Views.ProvisioningSelectEquipmentPageView();

				ConsumerController.showLayout(page, true, ["devices"]);
			},
			provisionNaming: function provisionNaming() {
				var page = new App.Consumer.SetupWizard.Provisioning.Views.ProvisioningNameEquipmentPageView();

				ConsumerController.showLayout(page, true, ["devices"]);
			},
			provisionCustomize: function provisionCustomize() {
				var page = new App.Consumer.SetupWizard.Provisioning.Views.ProvisioningCustomizeEquipmentPageView();

				ConsumerController.showLayout(page, true, ["devices"]);
			}
		};
	});

	return App.Consumer.SetupWizard.Controller;
});