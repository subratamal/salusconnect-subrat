"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusModal.view",
	"consumer/views/SalusAlertModal.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/models/SalusAlertModalModel"
], function (App, P, consumerTemplates, SalusPageMixin) {

	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn, $, _) {
		Views.SetupPairingPage = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/pairing"],
			className: "pairing-page",
			regions: {
				buttonRegion: ".bb-pairing-button"
			},
			initialize: function () {
				_.bindAll(this, "handleConnectClick");

				this.buttonView = new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "btn-warning preserve-case",
					clickedDelegate: this.handleConnectClick,
					buttonTextKey: "setupWizard.pairing.buttonText"
				});

				// todo : could be optimized by just getting properties for the coordinator
				this.deviceDependencies = App.salusConnector.getGatewayAndDelegates().map(function (device) {
					return device.getDeviceProperties();
				});
			},
			handleConnectClick: function () {
				var that = this;

				this.buttonView.showSpinner();
				App.getCurrentGateway().initiatePairingMode().then(function () {
					that.buttonView.hideSpinner();
					App.navigate("setup/provision");
				}).catch(function (err) {
					that.buttonView.hideSpinner();
					if (err === "No coordinator to initiate pairing") {
						App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
							model: new App.Consumer.Models.AlertModalViewModel({
								iconClass: "icon-warning",
								primaryLabelText: App.translate("setupWizard.pairing.noCoordinator"),
								secondaryLabelText: App.translate("setupWizard.pairing.requiresCoordinator"),
								leftButton: new App.Consumer.Views.ModalCloseButton({
									id: "ok-close-btn",
									classes: "width100 preserve-case",
									buttonTextKey: "common.labels.ok"
								})
							})
						}));

						App.showModal();
					}

					App.error(err);
				});
			},
 			onRender: function () {
				var that = this;

				P.all(this.deviceDependencies).then(function () {
					that.$("#bb-pairing-instruction-text").html(App.translate("setupWizard.pairing.paragraphHtml"));
					that.buttonRegion.show(that.buttonView);
				});
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "setupWizard",
			analyticsPage: "pairing"
		});
	});

	return App.Consumer.SetupWizard.Views.SetupPairingPage;
});