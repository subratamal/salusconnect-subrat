"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel"
], function (App, P, constants, config, consumerTemplates, SalusView, SalusPage, SalusButton, CheckboxView, CheckboxModel) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {
		Views.ProvisioningSelectEquipmentPageView = Mn.LayoutView.extend({
			template: consumerTemplates["setupWizard/provisioning/provisioningSelectEquipmentPage"],
			className: "provision-select-equipment-page",
			regions: {
				equipmentRegion: ".bb-equipment-region",
				cancelButtonRegion: ".bb-cancel-button",
				connectButtonRegion: ".bb-connect-button"
			},
			events: {
				"click .bb-equipment-region": "checkClick"
			},
			initialize: function () {
				_.bindAll(this, "_handleCancelClick", "_handleConnectClick", "checkClick", "proceedWithEquipment");

				this.gateway = App.getCurrentGateway();

				if (!this.gateway) {
					// get out
					return App.navigate("setup/pairing");
				}

				this.gateway.pairingDevices = [];

				this.permitJoinInterval = setInterval(this.setPermitJoinInterval, config.setPermitJoinRefresh);

				this.equipmentCompositeView = new Views.ProvisioningSelectEquipmentComposite({
					model: this.gateway
				});

				this.cancelButtonView = new SalusButton({
					id: "cancel-button",
					className: "min-width-btn btn btn-default",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this._handleCancelClick
				});

				this.connectButtonView = new SalusButton({
					id: "connect-button",
					classes: "min-width-btn btn btn-warning disabled",
					buttonTextKey: "setupWizard.provisioning.select.connect",
					clickedDelegate: this._handleConnectClick
				});
			},
			onRender: function () {
				this.equipmentRegion.show(this.equipmentCompositeView);
				this.cancelButtonRegion.show(this.cancelButtonView);
				this.connectButtonRegion.show(this.connectButtonView);
			},
			setPermitJoinInterval: function () {
				// every 100 seconds reset the setPermitJoinPeriod to 120 seconds
				App.getCurrentGateway().initiatePairingMode();
			},
			_handleCancelClick: function () {
				window.history.back();
			},
			_handleConnectClick: function () {
				var that = this, selectedEquipment = this.equipmentCompositeView.getCheckedItems();

				if (_.isArray(selectedEquipment) && selectedEquipment.length > 0) {
					this.equipmentCompositeView.toggleError(false);
					this.equipmentCompositeView.addSpinner();

					// new device setup
					// this promise chain entails adding the device to the device collection,
					// collecting a promise array of each new device getting it's properties and setting SetRefresh
					// it then deactivates pairing mode on the gateway
					// and lastly navigates away
					
                    return this.proceedWithEquipment(selectedEquipment).then(function (newlyProvisioned) {
						// disabled the button after proceeding
						that.connectButtonView.disable();
						clearInterval(that.equipmentCompositeView.devicesPoll);
                        
                        return  App.salusConnector.getFullDeviceCollection().load().then(function () {
                                var devices = App.salusConnector.getFullDeviceCollection();
                        
                        		var addPromises = _.map(newlyProvisioned, function (provisionDevice) {
								var deviceModel = App.salusConnector.getDevice(provisionDevice.device.key),
										propertyPromise;

								// loads each individually
								return deviceModel.getDevicePropertiesPromise().then(function () {
									if (deviceModel && deviceModel.get("SetRefresh_d")) {
										propertyPromise = deviceModel.setProperty("SetRefresh_d", 1);
									} else {
										App.warn("Did not SetRefresh on " + deviceModel.get("dsn"));
									}

									return propertyPromise || P.resolve();
								});
							});

							return P.all(addPromises);
                        
                      }).then(function () {
							// set pairing mode property to 0
							return App.getCurrentGateway().deactivatePairingMode();
						});;

					}).then(function () {
						that.equipmentCompositeView.removeSpinner();

						App.navigate("setup/naming");

					}).catch(function (err) {
						that.equipmentCompositeView.removeSpinner();

						return P.reject(err);
					});
				} else if (!this.equipmentCompositeView.collection.isEmpty()) {
					// only show error if there's actually equipment showing
					this.equipmentCompositeView.toggleError(true);
				}
			},
			checkClick: function (/*event*/) {
				if (this.equipmentCompositeView.anyChecked()) {
					this.connectButtonView.enable();
				} else {
					this.connectButtonView.disable();
				}
			},
			proceedWithEquipment: function (equipment) {
				var that = this, promiseArray = [];

				_.each(equipment, function (device) {
					// add the device to gateway
					promiseArray.push(that.gateway.pairDevice(device.get("dsn")));
				});

				return P.all(promiseArray);
			},
			onDestroy: function () {
				clearTimeout(this.equipmentCompositeView.devicesPoll);
				clearInterval(this.permitJoinInterval);

				App.getCurrentGateway().deactivatePairingMode();
			}
		}).mixin([SalusPage], {
			analyticsSection: "setupWizard",
			analyticsPage: "provisioningSelect"
		});

		// a device was found view
		Views.ProvisionSelectEquipmentItemView = Mn.ItemView.extend({
			template: consumerTemplates["setupWizard/provisioning/provisioningSelectEquipmentListItem"],
			className: "provision-device",
			ui: {
				icon: ".bb-device-icon",
				checkbox: ".bb-provision-checkbox"
			},
			initialize: function () {
				// get the i18nKey and cssClass for image
				var displayTextKey = constants.deviceTypeMapping(this.model);

				this.iconClass = constants.getProvisioningIconClass(this.model);

				this.checkboxView = new CheckboxView({
					model: new CheckboxModel({
						name: "provisionCheckbox",
						labelTextKey: displayTextKey,
						secondaryIconClass: "",
						isChecked: false
					})
				});
			},
			onRender: function () {
				this.ui.checkbox.append(this.checkboxView.render().$el);
				this.ui.icon.addClass(this.iconClass);
			},
			onDestroy: function () {
				this.checkboxView.destroy();
			}
		}).mixin([SalusView]);

		Views.CurrentlyScanningView = Mn.ItemView.extend({
			className: "currently-scanning",
			template: consumerTemplates["setupWizard/provisioning/currentlyScanningView"],
			ui: {
				text: ".bb-currently-scanning",
				spinner: ".bb-spinner"
			},
			onRender: function () {
				this.ui.text.text(App.translate("setupWizard.provisioning.select.currentlyScanning"));
			},
			/**
			 * stop the spinner and change the text
			 */
			stopSpinner: function () {
				this.ui.text.text(App.translate("setupWizard.provisioning.select.notFound"));
				this.ui.spinner.detach();
			}
		}).mixin([SalusView]);

		// equipment container composite
		Views.ProvisioningSelectEquipmentComposite = Mn.CompositeView.extend({
			template: consumerTemplates["setupWizard/provisioning/provisioningSelectEquipmentComposite"],
			className: "equipment-provision-list",
			childView: Views.ProvisionSelectEquipmentItemView,
			childViewContainer: ".bb-provision-list",
			emptyView: Views.CurrentlyScanningView,
			ui: {
				spinnerHook: ".bb-spinner-hook",
				error: ".bb-select-equipment-error"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "toggleError", "getCheckedItems", "anyChecked", "getDevicesToPair");

				if (!this.model) {
					return this;
				}

				// to be filled
				this.collection = new B.Collection();

				this.getDevicesToPair();
			},
			/**
			 * make call on model to scan and change the view
			 * if the call fails, it will scan again
			 * if the call succeeds, it will add to the collection and continue scan
			 */
			getDevicesToPair: function () {
				var that = this;

				if (this.isDestroyed) {
					return;
				}

				return this.model.getDevicesToPair().then(function (devicesToPair) {
					// don't add duplicates
					// don't use set because the collection is rerendered, defaulting the users' devices to unchecked
					_.each(devicesToPair, function (device) {
						if (!that.collection.findWhere({dsn: device.dsn})) {
							that.collection.add(device);
						}
					});

					return P.resolve(devicesToPair);
				}).finally(function () {
					that.devicesPoll = setTimeout(that.getDevicesToPair, config.provisionDevicesPollRefresh * 1000);
				});
			},
			getCheckedItems: function () {
				var array = [];

				if (!this.collection.isEmpty()) {
					_.each(this.children._views, function (childView) {
						if (childView.checkboxView.getIsChecked()) {
							array.push(childView.model);
						}
					});
				}

				return array;
			},
			/**
			 * see if any are checked so we can enable the button
			 * @returns {boolean}
			 */
			anyChecked: function () {
				if (!this.collection || this.collection.isEmpty()) {
					return false;
				} else {
					return _.some(this.children._views, function (childView) {
						return childView.checkboxView.getIsChecked();
					});
				}
			},
			toggleError: function (toggleBool) {
				this.ui.error.toggleClass("hidden", !toggleBool);
			},
			onDestroy: function () {
				clearTimeout(this.devicesPoll);
			},
			/**
			 * add spinner on connect button press
			 */
			addSpinner: function () {
				var $spinner = $(_.template('<div class="spinner center-block margin-t-10"></div>')());

				this.ui.spinnerHook.append($spinner);
			},
			removeSpinner: function () {
				if (this.ui.spinnerHook.children().length) {
					this.ui.spinnerHook.children().first().detach();
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Provisioning.Views.ProvisioningSelectEquipmentPageView;
});