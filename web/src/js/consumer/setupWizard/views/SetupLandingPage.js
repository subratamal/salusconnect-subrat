"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/setupWizard/mixins/mixin.setupWizardPage",
	"consumer/setupWizard/views/LandingInstructionBox.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusModal.view",
	"consumer/setupWizard/views/LanguageModal.view"
], function (App, consumerTemplates, SalusPageMixin, SetupWizardPageMixin) {

	App.module("Consumer.SetupWizard.Views", function (Views, App, B, Mn) {
		Views.SetupLandingPage = Mn.ItemView.extend({
			template: consumerTemplates["setupWizard/landing"],
			className: "landing-page",
			ui: {
				pageTitle: "#bb-page-tile",
				subHeader: "#bb-page-sub-header",
				instructionsContainer: "#bb-instruction-box-container"
			},
			initialize: function () {
				this.createProfileBox = new Views.SetupLandingInstructionBoxView({
					model: new B.Model({
						backgroundImageClass: "create-profile",
						i18nKey: "setupWizard.landing.createProfile",
						isActive: true,
                        isDisable: true
					}),
					clickedDelegate: function () {
						App.navigate("setup/profile");
					}
				});
                
                var btnConnectDisable = App.getCurrentGateway()? false:true;
				this.connectEquipmentBox = new Views.SetupLandingInstructionBoxView({
					model: new B.Model({
						backgroundImageClass: "equipment-connect",
						i18nKey: "setupWizard.landing.equipmentConnect",
						isActive: false,
                        isDisable: btnConnectDisable
					}),
					clickedDelegate: function () {
						App.navigate("setup/pairing");
					}
				});

                //this work need todo
//				if (App.isDevOrInt()) {
//					this.shareEquipmentBox = new Views.SetupLandingInstructionBoxView({
//						model: new B.Model({
//							backgroundImageClass: "equipment-share",
//							i18nKey: "setupWizard.landing.equipmentShare",
//							isActive: false,
//                            isDisable: false
//						}),
//						clickedDelegate: function () {
//							App.navigate("settings/manageDevices");
//						}
//					});
//				}
			},

			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices"]).then(function () {
					var hasGateway = App.hasCurrentGateway(), i18nKeyEnding = hasGateway ? "connect" : "gateway";

					// this is here and the others are in initialize because we need to wait to see if we have a gateway
					that.connectGatewayBox = new Views.SetupLandingInstructionBoxView({
						model: new B.Model({
							backgroundImageClass: "gateway-connect",
							i18nKey: "setupWizard.landing.gatewayConnect",
							isActive: hasGateway
						}),
						clickedDelegate: function () {
							App.navigate("setup/gateway");
						}
					});

					that.ui.pageTitle.text(App.translate("setupWizard.landing.header." + i18nKeyEnding));
					that.ui.subHeader.html(App.translate("setupWizard.landing.subHeaderHtml." + i18nKeyEnding));

					that.ui.instructionsContainer.append(that.createProfileBox.render().$el)
						.append(that.connectGatewayBox.render().$el)
						.append(that.connectEquipmentBox.render().$el);
                
                
                
                     

//					if (that.shareEquipmentBox) {
//						that.ui.instructionsContainer.append(that.shareEquipmentBox.render().$el);
//					}
				});
			},
			onDestroy: function () {
				if (this.connectGatewayBox) {
					this.connectGatewayBox.destroy();
				}

				if (this.createProfileBox) {
					this.createProfileBox.destroy();
				}

				if (this.connectEquipmentBox) {
					this.connectEquipmentBox.destroy();
				}

				if (this.shareEquipmentBox) {
					this.shareEquipmentBox.destroy();
				}
			},
			onShow: function () {
				App.salusConnector._aylaConnector.userFetchPromise.then(function () {
					var user = App.salusConnector.getSessionUser();

					if (!user.hasLanguage()) {
						App.modalRegion.show(new App.Consumer.Views.SalusModalView({
							backdrop: "static", // don't allow close
							contentView: new Views.LanguageModalView({
								model: user
							})
						}));

						App.showModal();
					}
				});
			}
		}).mixin([SalusPageMixin, SetupWizardPageMixin], {
			analyticsSection: "setupWizard",
			analyticsPage: "landing"
		});
	});

	return App.Consumer.SetupWizard.Views.SetupLandingPage;
});