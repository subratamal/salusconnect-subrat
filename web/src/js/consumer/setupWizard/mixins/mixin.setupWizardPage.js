"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.SetupWizard.Mixins", function (Mixins) {
		/**
		 * Adds a the setup wizard header image to the page.
		 * @constructor
		 */
		Mixins.SetupWizardPage = function () {
			this.after("render", function () {
				this.$el.prepend('<div class="setup-wizard-header"></div>').addClass("setup-wizard-page");
			});
		};
	});

	return App.Consumer.SetupWizard.Mixins.SetupWizardPage;
});