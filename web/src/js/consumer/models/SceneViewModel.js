"use strict";

define([
	"app"
], function (App) {

	App.module("Consumer.Models", function (Models, App, B) {
		Models.SceneViewModel = B.Model.extend({
			defaults: {
				name: null,
				icon: null, // TODO: this may move to it's own mapping
				selected: null,
				rules: null // //TODO: rules. id and name. will be an array
			}
		});

		Models.SceneViewModelCollection = B.Collection.extend({
			model: Models.SceneViewModel
		});
	});

	return App.Consumer.Models;
});