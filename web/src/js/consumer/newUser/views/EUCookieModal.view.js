"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view"
], function (App, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView) {

	App.module("Consumer.NewUser.Views", function (Views, App, B, Mn, $, _) {
		Views.EUCookieModalView = Mn.LayoutView.extend({
			className: "modal-body eu-cookie-modal-content",
			regions: {
				acceptButtonRegion: ".bb-accept-button-region"
			},
			template: consumerTemplates["newUser/EUCookieModal"],
			initialize: function (options) {
				_.bindAll(this, "handleAcceptClick");

				this.acceptBtn = new SalusPrimaryButtonView({
					id: "accept-cookies-btn",
					classes: "btn btn-default min-width-btn pull-right",
					buttonTextKey: "common.labels.accept",
					clickedDelegate: options.acceptClickDelegate || this.handleAcceptClick
				});
			},
			handleAcceptClick: function () {
				App.hideModal();
			},
			onRender: function () {
				this.acceptButtonRegion.show(this.acceptBtn);

				// todo: update this with real privacy policy link
				this.$("#bb-cookie-info").append(App.translate("setupWizard.newUserForm.cookieModal.information"));
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.NewUser.Views.EUCookieModalView;
});