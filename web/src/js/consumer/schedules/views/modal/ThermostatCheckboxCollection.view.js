"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/settings/views/EmptyList.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Schedules.Views.Modal", function (Views, App, B, Mn, $, _) {
		Views.ThermostatCheckedRow = Mn.LayoutView.extend({
			template: consumerTemplates["schedules/modal/thermostatCheckboxRow"],
			className: "thermostat-checkbox-row row",
			regions: {
				checkbox: ".bb-checkbox"
			},
			bindings: {
				".checkbox-text": {
					observe: "name"
				}
			},
			onRender: function (params) {
				var checked = this._setIsChecked(params.options.singleControl);

				this.checkbox.show(new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "equipmentCheckbox",
						nonKeyLabel: this.model.get("name"),
						secondaryIconClass: "",
						isChecked: checked
					})
				}));
			},
			_setIsChecked: function (singleControl) {
				var checked = false,
						that = this;
				_.each(singleControl.get("devices"), function(device) {
					if (device.EUID === that.model.getEUID()) {
						checked = true;
					}
				});

				return checked;
			}
		}).mixin([SalusView]);

		Views.ThermostatCheckboxCollection = Mn.CollectionView.extend({
			childView: Views.ThermostatCheckedRow,
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "schedules.edit.modal.emptyThermostats"
			},
			className: "thermostats-table",
			initialize: function (options) {
				this.singleControl = options.singleControl;
			},
			childViewOptions: function () {
				return {
					singleControl: this.singleControl
				};
			},
			getCheckedItemKeys: function () {
				var array = [];
				if (!this.collection.isEmpty()) {
					this.children.each(function (child) {
						if (child.checkbox.currentView.getIsChecked()) {
							array.push(child.model.get("key"));
						}
					});
				}
				return array;
			},
			getCheckedDevices: function() {
				var array = [];
				if (!this.collection.isEmpty()) {
					this.children.each(function (child) {
						if (child.checkbox.currentView.getIsChecked()) {
							array.push({
								EUID:child.model.getEUID(),
								oem_model:child.model.get("oem_model")
							});
						}
					});
				}
				return array;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Schedules.Views.Modal.ThermostatCheckboxCollection;
});