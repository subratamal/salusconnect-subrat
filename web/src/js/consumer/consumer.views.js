"use strict";

define([
	"app",
	"consumer/views/AuthenticatingPage.view",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusDropdown.view",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/SalusModal.view",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusTabbedSection.view",
	"consumer/views/SalusTextBox.view",
	"consumer/views/SalusTooltip.view"
], function (App) {

	return App.Consumer.Views;
});