"use strict";

define([
	"app",
	"tab",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, BootstrapTab, consumerTemplates, SalusView) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $) {
		/**
		 * expects options which contains an array of tab data,
		 * tab data is in the following form
		 * {
		 * 		key: string used to represent the tab (examples -> devices, groups)
		 * 		i18n: string i18n key for the header text
		 *  	view: instantiated view to be appended into the content
		 * }
		 */
		Views.SalusTabbedSectionView = Mn.ItemView.extend({
			template: consumerTemplates.salusTabbedSection,
			headerTemplate: consumerTemplates.salusTabHeader,

			events: {
				"click .bb-tab-header": "_handleAnchorClick"
			},

			initialize: function (options) {
				if (!options || !options.tabs) {
					throw Error("SalusTabbedSection.view must be passed tabs on initialization");
				}

				this.tabs = options.tabs;
				this.onTabChangeCb = options.onTabChangeCb;
			},

			onRender: function () {
				var that = this;
				var $headerArea = this.$(".bb-tab-header-area"),
					$contentArea = this.$(".bb-tab-content-area");

				this.tabs.forEach(function (tab, index) {
					tab.view.$el.addClass(tab.classes);

					$headerArea.append(that._buildTabHeaderDom(tab));
					$contentArea.append(tab.view.render().$el);
					tab.view.$el.data("view-index", index);
				});

				this._activeIndex = 0;
				this.$("li").first().addClass('active');
				this.$(".bb-tab-content").first().addClass('active');
			},

			getActiveView: function () {
				return this.tabs[this._activeIndex || 0].view;
			},

			_buildTabHeaderDom: function (tab) {
				var templateObj = {
					key: tab.key,
					title: App.translate(tab.i18n)
				};

				return this.headerTemplate(templateObj);
			},

			_handleAnchorClick: function (evt) {
				var $tabHeader = $(evt.currentTarget);

				this._activeIndex = this.$($tabHeader.attr("href")).data("view-index");

				evt.preventDefault();
				evt.stopPropagation();
				$tabHeader.tab("show");
				if (this.onTabChangeCb) {
					this.onTabChangeCb(evt, $tabHeader.attr('href').replace("#", ""));
				}
			},
			onDestroy: function () {
				this.tabs.forEach(function (tab) {
					tab.view.destroy();
				});
			}
		}).mixin([SalusView]);
	});



	return App.Consumer.Views.SalusTabbedSectionView;
});