"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App,  templates) {

	App.module("Consumer.Views.Mixins", function (Mixins) {
		Mixins.EquipmentPanel = function () {
			this.mixin([Mixins.SalusView]);

			this.setDefaults({
				template: templates["equipment/equipmentSettingsWidget"],
				className: "equipment-settings-widget",
				regions: {
					contentRegion: ".bb-widget-content-region"
				},
				ui: { // ui elements of the panel header
					title: ".bb-widget-header-text",
					iconLeft: ".bb-widget-header-icon-left",
					iconRight: ".bb-widget-header-icon-right",
					panelHeader: ".bb-widget-header",
					panel: ".panel"
				},
				_setupHeader: function () {
					if (!this.header) {
						this.ui.panel.addClass("no-header");
						return;
					}

					if (this.header.text) {
						this.ui.title.text(this.header.text);
					} else if (this.header.textKey) {
						this.ui.title.text(App.translate(this.header.textKey));
					}

					if (this.header.iconLeft) {
						this.ui.iconLeft.addClass(this.header.iconLeft);
					} else {
						this.ui.iconLeft.addClass("hidden");
					}

					if (this.header.iconRight) {
						this.ui.iconRight.addClass(this.header.iconRight);
					} else {
						this.ui.iconRight.addClass("hidden");
					}
				},
				_initCollapse: function () {
					var that = this;
					this.content.currentView.$el.addClass("collapse in");
					this.content.on("before:empty", function (view) {
						view.$el.off();
					});
					this.content.currentView.$el.on('hidden.bs.collapse', function () {
						that.content.$el.hide();
					});

					this.content.currentView.$el.on('show.bs.collapse', function () {
						that.content.$el.show();
					});
				},
				_toggleCollapseWidget: function () {
					this.content.currentView.$el.collapse('toggle');
				}
			});

			this.before("initialize", function (options) {
				this.header = options.header;
				this.$el.addClass(options.cssClass);
			});

			this.after("render", function () {
				this._setupHeader();
			});
		};
	});

	return App.Consumer.Views.Mixins.EquipmentPanel;
});