"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.FooterView = Mn.ItemView.extend({
			template: consumerTemplates["base/footer"],
			className: "container",
			ui: {
				copyrightDate: ".bb-copy-year"
			},
			onRender: function () {
				var year = new Date().getFullYear();
				this.ui.copyrightDate.text(year);
			}
		}).mixin([SalusView]);
	});


	return App.Consumer.Views.FooterView;
});