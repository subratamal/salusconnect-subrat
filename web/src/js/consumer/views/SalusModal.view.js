"use strict";

define([
	"app",
	"modal",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, BootstrapModal, consumerTemplates, SalusViewMixin ) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.SalusModalView = Mn.ItemView.extend({
			className: "modal fade",
			attributes: {
				"role": "dialog",
				"aria-labelledby": "modalLabel",
				"tabindex": "-1"
			},
			template: consumerTemplates.salusModal,
			initialize: function (options) {
				options = options || {};

				var that = this;

				if (!options.contentView) {
					throw new Error("Salus Modal requires a content view");
				}

				this.headerView = options.headerView;
				this.contentView = options.contentView;

				this.listenTo(this.contentView, "close:modal", this.hideModal);

				this.$el.on("shown.bs.modal", function () {
					that.$('#bb-center-btn-area').focus();
				});
			},
			showModal: function () {
				this.$el.modal('show');

				// Toggle arid-hidden to true so we can't tab outside the modal
				$("#bb-app-connected-solution").attr("aria-hidden", true);

				// Set the modal tab index to 0 so it joins the page's tab order
				this.$el.attr("tabindex", "0");
				this.$(".bb-modal-title").focus();
			},
			hideModal: function () {
				this.$el.modal('hide');

				// Set the modal tab index to -1 so it leaves the page's tab order
				this.$el.attr("tabindex", "-1");

				// Toggle arid-hidden to false to allow tabbing on the main app element
				$("#bb-app-connected-solution").attr("aria-hidden", false);
			},
            //Dev_SCS-3282 start
            hideClose: function(){
                $("#bb-icon-large-close").addClass("hidden");
            },
            //Dev_SCS-3282 end
			onRender: function () {
				this.$el.addClass(this.options.classes || "");

				if (this.options && this.options.size && this.$el.children()) {
					this.$el.children().first().addClass(this.options.size);
				}

				if (this.options && !!this.options.backdrop) {
					this.$el.attr("data-backdrop", this.options.backdrop);

					if (this.options.backdrop === "static") {
						this.$el.attr("data-keyboard", false); // no esc key
						this.$("#bb-icon-large-close").detach();
					}
				}

				if (this.headerView) {
					this.$(".bb-modal-content").append(this.headerView.render().$el);
				}

				this.$(".bb-modal-content").append(this.contentView.render().$el);

				this.$("#bb-icon-large-close").attr("aria-label", App.translate("common.labels.close"));
			},
			onBeforeDestroy: function () {
				this.$el.off();
			},
			onDestroy: function () {
				this.contentView.destroy();
				if (this.headerView) {
					this.headerView.destroy();
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Views.SalusModalView;
});
