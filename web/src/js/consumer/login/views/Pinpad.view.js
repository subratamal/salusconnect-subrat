"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/consumer.views"
], function (App, P, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $) {
		Views.PinpadView = Mn.ItemView.extend({
			id: "bb-pinpad-screen",
			className: "container pinpad-screen font-size-20",
			template: consumerTemplates["login/pinpad"],
			ui: {
				"pinMode": ".bb-pin-mode",
				"enteredNumbers": ".bb-entered-numbers",
				"pinStatus": ".bb-pin-status"
			},
			events: {
				"click .bb-pin-number-button": "_handlePinButtonPress",
				"click .bb-pin-backspace-button": "_handleBackspace",
				"click .bb-clear-pin-button": "_clearPin"
			},
			bindings: {
				".bb-pin-mode": "mode"
			},
			initialize: function () {
				var that = this;

				this._pinEntriesArray = [];

				// lets us use :active in styling for touch devices
				$('body').attr("ontouchstart", "");

				// check and see if there is a pin set on device, set mode accordingly
				// TODO: do not set mode this way in future
				this._havePinSet().then(function (result) {
					if (result) {
						that.model.set("mode", "authenticate");
					} else {
						that.model.set("mode", "create");
					}
				}).catch(function (e) {
					// TODO: remove catch
					App.error(e);

					return P.reject(e);
				});
			},
			/**
			 * handlePinButtonPress
			 * handles a numerical button press
			 * @param event
			 * @private
			 */
			_handlePinButtonPress: function (event) {
				var pinNum = parseInt($(event.currentTarget).data("pin-num"));

				this.ui.pinStatus.addClass("hidden");

				if (!isNaN(pinNum) && this._pinEntriesArray.length < 4) {
					this._pinEntriesArray.push(pinNum); // valid, add int to array
					this.ui.enteredNumbers.append(pinNum);
				}

				// check it when we have 4 numbers in array
				if (this._pinEntriesArray.length === 4) {
					this._handlePinSubmit();
				}
			},
			/**
			 * handleBackspace
			 * pops the pinEntriesArray and backspaces the text
			 * @private
			 */
			_handleBackspace: function () {
				if (this._pinEntriesArray.length > 0) {
					this._pinEntriesArray.pop();
					this.ui.enteredNumbers.text(this.ui.enteredNumbers.text().slice(0, -1));
				}
			},
			/**
			 * clearPin
			 * reset the pinEntriesArray and reset the text
			 * called as a helper function and also as event handler for 'Clear' button
			 * @private
			 */
			_clearPin: function () {
				this._pinEntriesArray = [];
				this.ui.enteredNumbers.text("");
			},
			/**
			 * handlePinSubmit
			 * called when the user enters 4 pin numbers
			 * either sets or verifies the pin depending on mode
			 * @returns {*}
			 * @private
			 */
			_handlePinSubmit: function () {
				var that = this;

				if (this.model.get("mode") === "create") {
					return this._setPin(this._pinEntriesArray).then(function (result) {
						if (result) {
							that.model.set("mode", "authenticate");
							that.ui.pinStatus.text(App.translate("login.pinpad.setSuccess"));
							that.ui.pinStatus.removeClass("hidden");
							that._clearPin();
						} else {
							that.ui.pinStatus.text(App.translate("login.pinpad.setFail"));
							that.ui.pinStatus.removeClass("hidden");
						}

						return result;
					}).catch(function (e) {
						// TODO: pin was not 4 elements or cordova call failed
						App.log(e);

						return P.reject(e);
					});
				} else if (this.model.get("mode") === "authenticate") {
					return this._checkPin().then(function (result) {
						if (result) {
							that.ui.pinStatus.text(App.translate("login.pinpad.authSuccess"));
							that.ui.pinStatus.removeClass("hidden");

							// TODO:trigger login flow - for now we just reset and let them authenticate again
							that._clearPin();
						} else {
							// reset what we have entered on incorrect
							that.ui.pinStatus.text(App.translate("login.pinpad.authInvalid"));
							that.ui.pinStatus.removeClass("hidden");
							that._clearPin();
						}

						return result;
					}).catch(function (e) {
						// TODO: remove catch - cordova failed
						App.error(e);

						return P.reject(e);
					});
				}
			},
			/**
			 * havePinSet
			 * makes cordova call to see if there is a pin set on the device
			 * used for setting the mode in initialize
			 * @returns {*}
			 * @private
			 */
			_havePinSet: function () {
				var that = this,
						promise;

				promise = new P.Promise(function (resolve, reject) {
					return that._makeCordovaCall(resolve, reject, "havePinSet", []);
				});

				return promise.then(function (result) {
					// call succeeds, returns the bool

					return result;
				}).catch(function (e) {
					App.error(e);

					return P.reject(e);
				});
			},
			/**
			 * setPin
			 * set the pin in mobile to the specified string given as argument.
			 * called when in "create" mode
			 * @returns {*}
			 * @private
			 */
			_setPin: function (pinArray) {
				var that = this,
						promise;

				if (this.model.get("mode") !== "create") {
					return false;
				}

				promise = new P.Promise(function (resolve, reject) {
					return that._makeCordovaCall(resolve, reject, "setPin", [pinArray]);
				});

				return promise.then(function (result) {
					// sets pin correctly, result is boolean value
					return result;
				}).catch(function (e) {
					// failed to set pin because not length 4 or cordova call fails
					App.error(e);

					return P.reject(e);
				});
			},
			/**
			 * checkPin
			 * verifies the entered pin against the one on the mobile device
			 * called when in 'authenticate mode'
			 * @returns {*}
			 * @private
			 */
			_checkPin: function () {
				var that = this,
						promise;

				if (this.model.get("mode") !== "authenticate") {
					return false;
				}

				promise = new P.Promise(function (resolve, reject) {
					return that._makeCordovaCall(resolve, reject, "checkPin", [that._pinEntriesArray]); // where to keep that data
				});

				return promise.then(function (result) {
					// check pin passes, result is bool
					return result;
				}).catch(function (e) {
					App.error(e);

					return P.reject(e);
				});
			},
			/**
			 * removePin
			 * Makes a cordova call to unset a pin on the device
			 * TODO: make use of this later
			 * @returns {*}
			 * @private
			 */
			_removePin: function () {
				var that = this,
						promise;

				promise = new P.Promise(function (resolve, reject) {
					return that._makeCordovaCall(resolve, reject, "removePin", []);
				});

				return promise.then(function (message) {
					// pin removed from device
					that.ui.pinStatus.text(App.translate("login.pinpad.pinRemoved"));
					that.ui.pinStatus.removeClass("hidden");
					that.model.set("mode", "create");
					that._clearPin();

					return message;
				}).catch(function (e) {
					// there was no pin on the device so couldn't remove or cordova call failed
					App.error(e);

					return P.reject(e);
				});
			},
			/**
			 * makeCordovaCall
			 * helper for cordova to interface with ios model
			 * we may end up using the one in SessionManager
			 * @param resolve
			 * @param reject
			 * @param method
			 * @param paramList
			 * @returns {*}
			 * @private
			 */
			_makeCordovaCall: function (resolve, reject, method, paramList) {
				if (!App.isCordovaApiReady(method)) {
					return false;
				}

				return window.cordova.exec(
						resolve, //callback handler
						reject, //error handler
						"PinpadPlugin", //interface with that plugin in iOS
						method, //method the plugin executes
						paramList //array containing params
				);
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Login.Views.PinpadView;
});
