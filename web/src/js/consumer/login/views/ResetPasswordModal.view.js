"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view"
], function (App, P, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView) {

	App.module("Consumer.Login.Views", function (Views, App, B, Mn, $, _) {
		Views.ResetPasswordModalView = Mn.LayoutView.extend({
			className: "modal-body",
			regions: {
				confirmationBtnRegion: "#bb-confirm-btn",
				forgotPasswordBtnRegion: "#bb-forgot-password-btn"
			},
			template: consumerTemplates["login/resetPasswordModal"],
			initialize: function () {
				_.bindAll(this, "handleConfirmationClick");

				this.confirmationBtn = new SalusPrimaryButtonView({
					id: "confirmation-btn",
					classes: "min-width-btn pull-right",
					buttonTextKey: "common.labels.finished",
					clickedDelegate: this.handleConfirmationClick
				});

				this.forgotPasswordBtn = new SalusPrimaryButtonView({
					id: "forgot-password-btn",
					classes: "min-width-btn",
					buttonTextKey: "login.login.forgotPassword",
					clickedDelegate: this.handleForgotPasswordClick
				});
			},
			handleConfirmationClick: function () {
				App.hideModal();
			},
			handleForgotPasswordClick: function () {
				App.navigate("login/forgotPassword");
				App.hideModal();
			},
			onRender: function () {
				this.confirmationBtnRegion.show(this.confirmationBtn);
				this.forgotPasswordBtnRegion.show(this.forgotPasswordBtn);
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Login.Views.ResetPasswordModalView;
});