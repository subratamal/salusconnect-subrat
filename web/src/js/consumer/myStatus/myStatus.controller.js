"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumer.controller",
	"consumer/myStatus/views/MyStatusPage.view",
	"consumer/myStatus/views/NewMyStatusPage.view",
	"consumer/myStatus/views/MakeMyStatusPage.view"
], function (App, constants, ConsumerController) {

	App.module("Consumer.MyStatus", function (MyStatus) {
		MyStatus.Controller = {
			myStatus: function (dsn) {
				var page = new App.Consumer.MyStatus.Views.MyStatusPageView();

				if (!dsn) {
					dsn = App.getCurrentGatewayDSN();

					if (dsn !== constants.noGateway) {
						App.changeRoute("myStatus/" + dsn);
					}
				}

				if (ConsumerController.changeGatewayIfNeeded(dsn, "myStatus")) {
					ConsumerController.showLayout(page, true, ["devices", "ruleGroups"]);
				}
			},

			newStatus: function (dsn) {
				var page = new App.Consumer.MyStatus.Views.NewMyStatusPageView();

				if (ConsumerController.changeGatewayIfNeeded(dsn, "myStatus")) {
					ConsumerController.showLayout(page, true, ["devices", "ruleGroups"]);
				}
			},

			makeStatus: function (dsn) {
				var page = new App.Consumer.MyStatus.Views.MakeMyStatusPageView();

				if (ConsumerController.changeGatewayIfNeeded(dsn, "myStatus")) {
					ConsumerController.showLayout(page, true, ["devices", "ruleGroups"]);
				}
			}
		};
	});

	return App.Consumer.MyStatus.Controller;
});