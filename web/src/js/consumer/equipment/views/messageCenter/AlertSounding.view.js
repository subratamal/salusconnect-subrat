"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, templates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.AlertSoundingView = Mn.ItemView.extend({
			template: templates["equipment/messageCenter/alertSounding"]

		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.AlertSoundingView;
});