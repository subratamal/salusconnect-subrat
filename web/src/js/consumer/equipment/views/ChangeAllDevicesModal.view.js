"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view"
], function (App, P, consumerTemplates, SalusViewMixin, SalusPrimaryButtonView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.ChangeAllDevicesModalView = Mn.LayoutView.extend({
			className: "modal-body",
			regions: {
				changeAllBtnRegion: "#bb-btn-change-all",
				cancelOneBtnRegion: "#bb-btn-cancel"
			},
			template: consumerTemplates["equipment/changeAllDevicesModal"],
			initialize: function (options) {
				_.bindAll(this, "handleChangeAllClick", "handleCancelClick");

				this.modelType = options.modelType;
				this.devices = options.devices;

				this.changeAllBtn = new SalusPrimaryButtonView({
					id: "change-all-btn",
					classes: "min-width-btn",
					clickedDelegate: options.okClickedDelegate || this.handleChangeAllClick
				});

				this.cancelBtn = new SalusPrimaryButtonView({
					id: "cancel-btn",
					classes: "min-width-btn",
					buttonTextKey: options.cancelTextKey || "common.labels.cancel",
					clickedDelegate:  options.cancelClickedDelegate || this.handleCancelClick
				});
			},
			handleCancelClick: function () {
				App.hideModal();
			},
			handleChangeAllClick: function () {

				// todo : update all devices in devices here
				this._notImplementedAlert();

				App.hideModal();
			},
			onRender: function () {
				this.changeAllBtnRegion.show(this.changeAllBtn);
				this.cancelOneBtnRegion.show(this.cancelBtn);

				// dynamic title changes
				this.$("#change-all-title").text(App.translate("equipment.dialog.titleFormatted",
						{"modelTypeName": App.translate("equipment.names." + this.modelType)}));

				this.$("#change-all-btn").text(App.translate("equipment.dialog.changeAllDevicesFormatted",
						{"modelTypeName": App.translate("equipment.names." + this.modelType)}));
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Equipment.Views.ChangeAllDevicesModalView;
});