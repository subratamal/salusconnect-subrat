"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.HoldTempPointModal = Mn.ItemView.extend({
			template: consumerTemplates["equipment/holdTempPointModal"],
			className: "bb-hold-temp-point hold-temp-point-modal",
			ui: {
				radioOptionChangeTemp: "#optionChangeTemp",
				radioOptionScheduleChange: "#optionScheduleChange",
				okModalBtn : ".bb-ok-btn",
				checkBoxHideHoldPointModal:".bb-hide-hold-temp-set-modal-checkbox"
			},
			events: {
				'click @ui.okModalBtn':"_saveHoldPointOptions"
			},
			initialize: function(/* options */) {
				_.bindAll(this, "_saveHoldPointOptions");
			},
			_saveHoldPointOptions: function() {
				var that = this,
					checkBoxValue = this.ui.checkBoxHideHoldPointModal.is(":checked"),
					modelCheckBoxValue = this.model.get("hide_set_hold_point_modal");

				if (!modelCheckBoxValue && checkBoxValue) {
					this.model.set("hide_set_hold_point_modal", checkBoxValue);
				}

				// If one of our properties was updated, trigger a model save
				this.model.persist().then(function () {
					that.trigger("modal:closed", that);
				});

			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.HoldTempPointModal;
});