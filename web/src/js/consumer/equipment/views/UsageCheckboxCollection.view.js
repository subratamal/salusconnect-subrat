"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/settings/views/EmptyList.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, _) {

		Views.UsageCheckbox = Mn.LayoutView.extend({
			template: consumerTemplates["equipment/usageCheckbox"],
			className: "device-checkbox col-xs-6",
			regions: {
				checkboxRegion: "#bb-checkbox"
			},
			onRender: function () {
				this.checkbox = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						name: "usageCheckbox",
						id: this.model.get("id"),
						nonKeyLabel: this.model.get("id"),
						secondaryIconClass: "",
						checkColor: this.model.get("color") || "",
						isChecked: this.model.get("isChecked")
					})
				});

				this.checkboxRegion.show(this.checkbox);

				this.listenTo(this.checkbox, "clicked:checkbox", this._handleCheckboxClicked);
			},
			_handleCheckboxClicked: function (checkboxModel) {
				this.trigger("trigger:checkboxClicked", checkboxModel);
			}
		}).mixin([SalusView]);

		Views.UsageCheckboxCollectionView = Mn.CollectionView.extend({
			childView: Views.UsageCheckbox,
			className: "usage-checkbox-area",
			childEvents: {
				"trigger:checkboxClicked": "_handleCheckboxClicked"
			},
			_handleCheckboxClicked: function () {
				var checkedDevices = this.getCheckedDevices();

				this.trigger("trigger:checkboxClicked", checkedDevices);
			},
			getCheckedDevices: function () {
				var array = [];

				if (!this.collection.isEmpty()) {
					_.each(this.children._views, function (childName, childView) {
						if (childView.checkbox.model.get("isChecked")) {
							array.push(childView.checkbox.model.get("id"));
						}
					});
				}

				return array;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.UsageCheckboxCollectionView;
});