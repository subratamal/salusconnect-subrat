"use strict";

define([
	"app",
	"momentWrapper",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/models/HistoryTable.model"
], function (App, moment, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $) {

		// DUMMY DATA HELPERS
		/*var DummyHistoryModel = B.Model.extend({
			defaults: {
				dateTime: null,
				user: null,
				event: null
			}
		});

		var DummyHistoryCollection = B.Collection.extend({
			model: DummyHistoryModel
		});

		var createDummyHistoryCollect = function () {
			var initArray = [];

			for(var i = 0; i < 10; i++) {
				initArray.push({
					dateTime: new Date(Math.random() + i),
					user: "Jerry Rice" + i,
					event: "Something changed" + i
				});
			}

			return new DummyHistoryCollection(initArray);
		};*/

		Views.EquipmentHistoryRow = Mn.ItemView.extend({
			template: consumerTemplates["equipment/widgets/equipmentHistoryRow"],
			className: "row equipment-history-row",
			bindings: {
				".bb-history-date": {
					observe: "dateTime",
					onGet: function (value) {
						return moment(value).format("MMM DD");
					}
				},
				".bb-history-time": {
					observe: "dateTime",
					onGet: function (value) {
						return moment(value).format("hh:mma");
					}
				},
				".bb-history-user": "user",
				".bb-history-event": "event"
			}
		}).mixin([SalusView]);

		Views.EquipmentHistoryWidget = Mn.CompositeView.extend({
			template: consumerTemplates["equipment/widgets/equipmentHistoryWidget"],
			className: "equipment-history-widget col-xs-12",
			childView: Views.EquipmentHistoryRow,
			childViewContainer: "#bb-equipment-history-table",
			events: {
				'click .bb-header': "headerClick",
				'click .bb-maximize': "_notImplementedAlert"
			},
			initialize: function () {
				this.model = new App.Consumer.Equipment.Models.HistoryTableModel();
				this.collection = new B.Collection(); //createDummyHistoryCollect();

				this.listenTo(this.model, 'change', this.resortView);
			},

			onRender: function () {
				this.$(".sort-arrow-icon").removeClass("show-arrow point-arrow-up");
				this.$('div[data-sort="' + this.model.get("sortBy") + '"] .sort-arrow-icon').addClass("show-arrow");
			},

			attachHtml: function(collectionView, childView, index){
				if (index < this.model.get("maxTableItems")) {
					this._defaultAttachHtml(collectionView, childView, index);
				}
			},

			// this is default Marionette Code, DO NOT EDIT
			_defaultAttachHtml: function (collectionView, childView, index) {
				if (collectionView.isBuffering) {
					// buffering happens on reset events and initial renders
					// in order to reduce the number of inserts into the
					// document, which are expensive.
					collectionView._bufferedChildren.splice(index, 0, childView);
				}
				else {
					// If we've already rendered the main collection, append
					// the new child into the correct order if we need to. Otherwise
					// append to the end.
					if (!collectionView._insertBefore(childView, index)){
						collectionView._insertAfter(childView);
					}
				}
			},

			headerClick: function (event) {
				var $el = $(event.currentTarget),
						clickedType = $el.data("sort"),
						currentType = this.model.get("sortBy");

				if (clickedType === currentType) {
					var newSortDescVal = !this.model.get("sortDesc");

					this.model.set("sortDesc", newSortDescVal);
					// this can be done with the the double
					this._flipSortArrow(!newSortDescVal);
				} else {
					this.model.set("sortBy", clickedType);
					this.model.set("sortDesc", true);
				}
			},

			viewComparator: function (historyA, historyB) {
				var sortDesc = this.model.get("sortDesc"),
						sortType = this.model.get("sortBy"),
						sortValueA = this._getSortTableValue(historyA, sortType),
						sortValueB = this._getSortTableValue(historyB, sortType),
						returnVal = 0;

				if (sortValueA > sortValueB) {
					returnVal = 1;
				} else if (sortValueA < sortValueB) {
					returnVal = -1;
				}

				return sortDesc ? returnVal : -1 * returnVal;
			},

			_flipSortArrow: function (shouldPointUp) {
				this.$(".sort-arrow-icon.show-arrow").toggleClass("point-arrow-up", shouldPointUp);
			},

			_getSortTableValue: function (model, sortType) {
				if (sortType === "TIME") {
					return model.get("dateTime").getTime();
				}

				return model.get(this.model.sortByEnum[sortType]);
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.EquipmentHistoryWidget;
});