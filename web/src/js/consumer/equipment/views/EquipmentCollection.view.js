"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusCheckbox.view",
	"consumer/settings/views/EmptyList.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {

		Views.EquipmentRow = Mn.LayoutView.extend({
			template: consumerTemplates["equipment/equipmentCollectionRow"],
			className: "equipment-row row",
			regions: {
				checkbox: ".bb-checkbox"
			},
			initialize: function () {
				this.isChecked = false;
                
                this.single = this.options.single;
                this.windowSensorRule=this.options.windowSensorRule;

				if (!!this.options.groupId) {
					var group = App.salusConnector.getGroup(this.options.groupId);

					this.isChecked = group.hasDevice(this.model.get("key"));
				} 
                // Dev_SBO-606
                else if(this.windowSensorRule){
                    if(this.windowSensorRule.get("conditions").findWhere({EUID:this.model.getEUID()})){
                        this.isChecked=true;
                    }
                }
			},
			onRender: function () {
				// show an icon next to the checkbox when we display all devices, not just thermostats
				var secondaryIconClass = this.options.deviceType === "all" ? "equipment-icon" : "";
                
                if(this.single){
                    this.checkbox.show(new App.Consumer.Views.RadioView({
                        model: new App.Consumer.Models.RadioViewModel({
                            name: "equipmentRadio",
                            value: this.model.get("key"),
                            isChecked: false,
                            nonKeyLabel: this.model.getDisplayName(),
                        })
                    }));
                } else {
                    this.checkbox.show(new App.Consumer.Views.CheckboxView({
                        model: new App.Consumer.Models.CheckboxViewModel({
                            name: "equipmentCheckbox",
                            nonKeyLabel: this.model.getDisplayName(),
                            secondaryIconClass: secondaryIconClass,
                            isChecked: this.isChecked
                        })
                    }));
                }

				if (secondaryIconClass) {
					this.$(".equipment-icon").css("background-image", "url(" + this.model.get("equipment_page_icon_url") + ")");
				}
			}
		}).mixin([SalusView]);

		Views.EquipmentCollectionView = Mn.CollectionView.extend({
			childView: Views.EquipmentRow,
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "equipment.myEquipment.newGroup.empty"
			},
			className: "equipment-table",
			childViewOptions: function () {
				return {
					groupId: this.options.groupId,
					deviceType: this.options.deviceType,
                    single: this.options.single,
                    // Dev_SBO-606
                    windowSensorRule: this.options.windowSensorRule
				};
			},
			getCheckedItemKeys: function () {
				var array = [];

                if(this.options.single){
                    array.push(parseInt(this.$("input:checked").val()));
                } else {
                    if (!this.collection.isEmpty()) {
                        this.children.each(function (childView) {
                            if (childView.checkbox.currentView.getIsChecked()) {
                                var test=childView.model.get("key");
                                array.push(childView.model.get("key"));
                            }
                        });
                    }
                }
                
				return array;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.EquipmentCollectionView;
});