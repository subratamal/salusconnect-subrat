"use strict";

define([
	"app",
	"consumer/consumer.controller",
	"consumer/equipment/views/MyEquipmentPage.view",
	"consumer/equipment/views/messageCenter/MessageCenter.view",
	"consumer/equipment/views/NewGroupPage.view",
	"consumer/equipment/views/GroupPage.view",
	"consumer/equipment/views/EquipmentPage.view",
	"consumer/equipment/views/CategoryPage.view",
	"consumer/equipment/views/tsc/ThermostatSingleControlPage.view"
], function (App, ConsumerController) {

	App.module("Consumer.Equipment", function (Equipment) {
		Equipment.Controller = {
			myEquipment: function myEquipment() {
				var EquipmentPageView = new App.Consumer.Equipment.Views.MyEquipmentPageView();

				ConsumerController.showLayout(EquipmentPageView, true, ["devices", "groups"]);
			},

			messageCenter: function messageCenter() {
				var MessageCenterView = new App.Consumer.Equipment.Views.MessageCenterView();

				ConsumerController.showLayout(MessageCenterView, true);
			},

			newGroup: function newGroup() {
				var page = new App.Consumer.Equipment.Views.NewGroupPageView();

				ConsumerController.showLayout(page, true, ["devices", "groups"]);
			},

			editGroup: function editGroup(groupId) {
				var page = new App.Consumer.Equipment.Views.NewGroupPageView({
					isEdit: true,
					key: groupId
				});

				ConsumerController.showLayout(page, true, ["devices", "groups"]);
			},

			group: function group(groupId) {
				var page = new App.Consumer.Equipment.Views.GroupPageView({key: groupId});
				ConsumerController.showLayout(page, true, ["devices", "groups"]);
			},

			equipment: function equipment(dsn) {
				var page = new App.Consumer.Equipment.Views.EquipmentPageView({dsn: dsn});

				ConsumerController.showLayout(page, true, ["devices"]);
			},

			category: function category(type) {
				var page = new App.Consumer.Equipment.Views.CategoryPageView({type: type});
				ConsumerController.showLayout(page, true, ["devices"]);
			},

			tsc: function tsc() {
				var page = new App.Consumer.Equipment.Views.ThermostatSingleControlPageView();

				ConsumerController.showLayout(page, true, ["devices", "tscs"]);
			}
		};
	});

	return App.Consumer.Equipment.Controller;
});