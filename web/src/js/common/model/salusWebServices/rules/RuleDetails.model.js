"use strict";

define([
	"app"
], function (App) {

	App.module("Models", function (Models, App, B) {
		Models.RuleCondition = B.Model.extend({
			defaults: {
				type: null

				/*
				 // day of week
				 dayPattern: null, (array)

				 // time of day
				 startTime: null, // (jsdate)
				 endTime: null, // (jsdate)

				 // property value
				 gt: null, // (int)
				 lt: null, // (int)
				 propName: null // (string)
				 deviceEUID: null // (string)

				 // press a button
				 ruleKey: null // (string)
				 */
			}
		});

		Models.RuleAction = B.Model.extend({
			defaults: {
				type: null

				/*
					// sms
					recipients: null, // (array)
					message: null, // (string)

					//email
					recipients: null, // (array)
					message: null, // (string)

					//propChange
					(device) EUID: null, // (string)
					propName: null, // (string)
					newValue: null // (inherent)
				 */
			}
		});

		Models.RuleDelayedAction = B.Model.extend({
			defaults: {
				types: null
				// todo: add model information here
			}
		});

		Models.RuleConditionCollection = B.Collection.extend({
			model: Models.RuleCondition
		});

		Models.RuleActionCollection = B.Collection.extend({
			model: Models.RuleAction
		});

		Models.RuleDelayedActionsCollection = B.Collection.extend({
			model: Models.RuleDelayedAction
		});
	});

	return App.Models;
});
