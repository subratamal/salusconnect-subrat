"use strict";

define([
	"app",
	"momentWrapper"
], function (App, moment) {

	App.module("Models", function (Models, App, B) {
		var momentMonthMap = {
			"0": "january",
			"1": "february",
			"2": "march",
			"3": "april",
			"4": "may",
			"5": "june",
			"6": "july",
			"7": "august",
			"8": "september",
			"9": "october",
			"10": "november",
			"11": "december"
		};

		Models.NotificationModel = B.Model.extend({
			defaults: {
				title: null,
				text: null,
				dateTime: null
			}
		});

		// TODO hook up to mike API with fetch when implemented
		Models.NotificationCollection = B.Collection.extend({
			model: Models.NotificationModel,

			toDisplayCollection: function (monthsBack) {
				var dateRightNow = new Date(),
					frontDate = moment(dateRightNow).endOf('month'),
					backDate = moment(dateRightNow).endOf('month').subtract(1, 'months'),
					outArray = [];

				for (var i = 0; i < monthsBack; i++) {
					var outData = {
						subCollection: this._filterBetweenDate(frontDate, backDate),
						displayModel: new B.Model({ month: momentMonthMap[frontDate.month()] })
					};

					outArray.push(new B.Model(outData));
					frontDate.subtract(1, 'months');
					backDate.subtract(1, 'months');
				}

				return new B.Collection(outArray);
			},

			// TODO remove when hooked up to api
			fillWithCannedData: function () {
				var loremIpsumText = "Lorem ipsum dolor sit amet, sit neque justo sem, sed semper donec at, vitae velit mauris nunc," +
					" mauris praesent mi ac. Vel justo vestibulum magna dis, ante risus lectus ac. Sed justo pulvinar eget ligula, amet quis nunc ac, diam rhoncus vestibulum elit. Nostrud adipiscing enim donec, amet volutpat imperdiet ipsum, sit mattis ac ut nec. " +
					"Fusce ligula neque augue erat, vitae nulla penatibus donec sem. Ut tortor lectus ultricies, per eros posuere sit. Massa enim imperdiet cras eu, cursus sagittis dui vitae. Et eu lorem velit, velit curabitur non vestibulum lacinia, nec adipiscing pretium eu. Ipsum tristique massa quisque, a non luctus libero et, sapien at consectetuer sapien. " +
					"Mollis aliquam sed velit, eros congue pulvinar ut a, pellentesque dolor ut tempus etiam, laoreet pede nulla nec purus.";

				for (var i = 0; i < 3; i++) {
					var date = new Date(),
						lastMonthDate = moment(date).subtract(1, "months").toDate();

					this.add({
						text: loremIpsumText,
						title: "Software Update",
						dateTime: date
					});

					this.add({
						text: loremIpsumText,
						title: "Software Update",
						dateTime: lastMonthDate
					});
				}
			},

			_filterBetweenDate: function (frontDate, backDate) {
				var filtered = this.filter(function (model) {
					return moment(model.get("dateTime")).isBetween(backDate, frontDate);
				});

				return new Models.NotificationCollection(filtered);
			}
		});
	});

	return App.Models.NotificationCollection;
});