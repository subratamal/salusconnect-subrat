"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.COMonitor = B.Model.extend({
			tileType: "coMonitor",
			modelType: constants.modelTypes.CARBONMONOXIDEDETECTOR,
			defaults: {
				// get properties
				FirmwareVersion: null, // string, Firmware Version
				MACAddress: null, // string, IEEE MAC Address
				SubDeviceName_c: null, // string, Sub device name _c naming is as defined on device wiki
				DeviceType: null, // integer, 1026 CO
				ManufactureName: null, // string, Manufacture Name
				ModelIdentifier: null, // string, Model ID
				PowerSource: null, // 1 ac 3 battery
				ErrorIASZSAlarmed1: null, //bool alarm 1
				ErrorIASZSAlarmed2: null, // bool alarm 2
				ErrorIASZSTampered: null, //tampered
				ErrorIASZSLowBattery: null, //low battery
				ErrorIASZSTrouble: null, //trouble/failure
				ErrorIASZSACFault: null, // ac/mains fault

				// set properties
				SetIndicator: null
			},
			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 1026,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_co_monitor.svg"),
					device_category_type: constants.categoryTypes.CARBONMONOXIDEDETECTORS,
					device_category_name_key: "equipment.myEquipment.categories.types.carbonMonoxideDetectors"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_co_monitor_grey.svg")
				});
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.COMonitor;
});
