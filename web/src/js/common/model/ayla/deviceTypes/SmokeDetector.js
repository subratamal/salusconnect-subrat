"use strict";

define([
	"app",
	"common/constants",
	"common/AylaConfig",
	"common/model/ayla/deviceTypes/mixin.BaseDevice"
], function (App, constants, AylaConfig, BaseAylaDevice) {

	App.module("Models", function (Models, App, B) {
		/**
		 * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
		 */
		Models.SmokeDetector = B.Model.extend({
			tileType: "smokeDetector",
			modelType: constants.modelTypes.SMOKEDETECTOR,
			defaults: {
				// get properties
				FirmwareVersion: null, //string
				MACAddress: null, // string, IEEE MAC Address
				SubDeviceName_c: null, // string, Sub device name _c naming is as defined on device wiki
				DeviceType: null, // integer, 1026 smoke detector
				ManufactureName: null, // string, Manufacture Name
				ModelIdentifier: null, // string, Model ID
				PowerSource: null, // 1 ac 3 battery
				ErrorIASZSAlarmed1: null, // bool, alarm1 or opened
				ErrorIASZSAlarmed2: null, // bool, alarm2 or opened
				ErrorIASZSTampered: null, // bool, tampered
				ErrorIASZSLowBattery: null, // bool, low battery
				ErrorIASZSTrouble: null, // bool, trouble or failure
				ErrorIASZSACFault: null, // bool, ac/mains fault

				// set properties
				SetIndicator: null // integer, set device identification indicator
			},

			initialize: function (/*data, options*/) {
				//TODO Setup
				this._setCategoryDefaults();
				this._setEquipmentPageDefaults();
			},
			_setCategoryDefaults: function () {
				this.set({
					device_category_id: 1026,
					device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_smoke_detector.svg"),
					device_category_type: constants.categoryTypes.SMOKEDETECTORS,
					device_category_name_key: "equipment.myEquipment.categories.types.smokeDetectors"
				});
			},
			_setEquipmentPageDefaults: function () {
				this.set({
					equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_smoke_detector_grey.svg")
				});
			}
		}).mixin([BaseAylaDevice]);
	});

	return App.Models.SmokeDetector;
});
