"use strict";
define([
	"app",
	"common/model/ayla/AylaScheduleCollection"
], function (App) {
	App.module("Models", function (Models) {
		Models.AylaScheduleMixin = function () {
			this.before("initialize", function () {
				this.set("schedules", new Models.AylaScheduleCollection(null, {
					deviceId: this.get("key")
				}));
				
				this._hasLoadedSchedules = false;
			});
			
			this.before("destroy", function () {
				this._schedulePromise = null;
			});

			this.setDefaults({
				hasLoadedSchedules: function () {
					return this._hasLoadedSchedules;
				},

				loadSchedules: function (isLowPriority) {
                    //固定true
                    isLowPriority=true;
                    var that=this;
					this._schedulePromise = this.get("schedules").refresh(isLowPriority);
					
					this._schedulePromise.then(function () {
						that._hasLoadedSchedules = true;
					});

					return this._schedulePromise;
				},
				processScheduleType: function (propName) {
					if (propName.toLowerCase().indexOf("mode") >= 0) {
						return "OFF";
					} else {
						return "ON";
					}
				},
				getSchedules: function () {
					return this.get("schedules");
				},

				getSchedulePromise: function () {
					if (this._schedulePromise) {
						return this._schedulePromise;
					}

					return this.loadSchedules();
				}
			});
		};

		return Models.AylaScheduleMixin;
	});
});