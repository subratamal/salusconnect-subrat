"use strict";

define([
	"app",
	"bluebird"
], function (App, P) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		/**
		 * This represents a grouping of ayla properties, all with the same display name
		 * this is generally used for the energy meter.
		 */
		Models.MulitplePropertyCollection = B.Collection.extend({
			isLinkedDeviceProperty: false,

			refresh: function (propId) {
				return this.getIndiviualProp(propId).refresh();
			},

			getProperty: function (propId) {
				// check that for eMeters, if we are actually looking at a single property instead of a multipleProperty
				if (this.length === 1) {
					return this.first().getProperty();
				}

				var property = this._getInnerProperty(propId);

				if (property) {
					return property.getProperty();
				} else {
					return this.getAllProperty();
				}
			},

			getDatapoints: function (propId) {
				if (this.length === 1) {
					return this.first().getDatapoints();
				}

				var property = this._getInnerProperty(propId);

				if (property) {
					return property.getDatapoints();
				} else {
					return this.getAllDatapoints();
				}
			},

			setProperty: function (value, propId, indexesToSet) {
				var that = this;
				if (this.length === 1) {
					return this.first().setProperty(value);
				}

				var property = this._getInnerProperty(propId);

				if (_.isArray(indexesToSet)) {
					return _.map(indexesToSet, function (index) {
						return that.find(function (prop) {
							return prop.get("name").indexOf("ep_" + index) >= 0;
						}).setProperty(value);
					});
				}

				if (property) {
					return property.setProperty(value);
				} else {
					return this.setAllProperties(value);
				}
			},

			refreshAll: function () {
				var promiseArray = this.map(function (property) {
					return property.refresh();
				});

				return P.all(promiseArray);
			},

			getAllProperty: function () {
				return this.map(function (property) {
					return property.getProperty();
				});
			},

			getAllDatapoints: function () {
				return this.map(function (property) {
					return property.getDatapoints();
				});
			},

			setAllProperties: function (value) {
				var promiseArray =  this.map(function (property) {
					return property.setProperty(value);
				});

				return P.all(promiseArray);
			},

			addOrUpdate: function (key, args, Class, identifier) {
				var prop = this.getIndiviualProp(identifier, identifier === key);

				if (prop) {
					prop.updateData.apply(null, args);
				} else {
					this.add(new Class(args[0], args[1]));
				}
			},

			getIndiviualProp: function (propId, overrideProp) {
				if (overrideProp) {
					return this.first();
				}

				if (_.isString(propId) ) {
					var splits = propId.split("_");
					propId = parseInt(splits[splits.length -1]);
				}

				return this.find(function (prop) {
					var splits = prop.get("name").split(":")[0].split("_");
					return splits[splits.length -1] === propId.toString();
				});
			},

			anyEqual: function (value) {
				return this.some(function (prop) {
					return prop.get("value") === value;
				});
			},

			_getInnerProperty: function (propId) {
				var prop = this.findWhere({
					key: propId
				});

				if (!propId || !prop) {
					return false;
				}

				return prop;
			}
		});
	});

	return App.Models.LinkedDevicePropertyModel;
});
