"use strict";

/**
 * This mixin is for all models backed by Ayla
 */
define([
	"app",
	"underscore",
	"backbone",
	"bluebird"
], function (App, _, B, P) {

	/**
	 * build defaults allows us to augment the various URL patterns onto defaults.
	 * @param options
	 * @returns {{}}
	 */
	var buildDefaults = function (options) {
		var out = {}, fetch, save, create;
		fetch = _.template(options.metaDataFetchUrl || "");					// the fetch URL
		save = options.metaDataSaveUrl ? _.template(options.metaDataSaveUrl) : fetch;			// the save url (or use get url if no save is provided)
		create = options.metaDataCreateUrl ? _.template(options.metaDataCreateUrl) : save;	// the create url (or use get url if no save is provided)

		out._metaDataFetchUrl = fetch;
		out._metaDataSaveUrl = save;
		out._metaDataCreateUrl = create;

		return out;
	};

	/**
	 * wires up a model to be backed by Ayla. This adds helpers to manage a lot of the
	 * data syncing and authentication
	 *
	 * @param options an object with configuration settings for the mixin
	 *        unpromisable: true|false -- prevent this model from being constructed with a promise return.
	 */
	var MetaDataMixin = function (options) {
		this.mixin([
		], options);

		var defaults = {
			fetchMetaData: function (isLowPriority) {
				var that = this;
				// first time we download meta data always place it in the fast name as it contains device names
				isLowPriority = this._metaDataFetchPromise ? isLowPriority : false;
				this._metaDataFetchPromise = App.salusConnector.makeAjaxCall(this._metaDataFetchUrl(this.toJSON()), null, "GET", null, { isLowPriority: isLowPriority }, [404]).then(function (metaData) {
					that.set(JSON.parse(metaData.datum.value));
					return that;
				}).catch(function (err) {
					if (err.status === 404) {
						return that._makeMetaDataProp().then(that.fetchMetaData);
					}

					throw err;
				});

				return this._metaDataFetchPromise;
			},

			_persistMetaData: function () {
				var payload, that = this;
				if (this._metaDataProperties) {
					payload = {
						datum: {
							key: "metaData",
							value: JSON.stringify(this.pick(this._metaDataProperties))
						}
					};
					return App.salusConnector.makeAjaxCall(this._metaDataSaveUrl(this.toJSON()), payload, "PUT", null, null, [404]).catch(function (err) {
						if (err.status === 404) {
							return that._makeMetaDataProp();
						}

						throw err;
					});
				}

				return P.resolve("No Meta Data Properties");
			},

			_makeMetaDataProp: function () {
				var payload = {
					datum: {
						key: "metaData",
						value: JSON.stringify(this.pick(this._metaDataProperties))
					}
				};

				return App.salusConnector.makeAjaxCall(this._metaDataCreateUrl(this.toJSON()), payload, "POST");
			},
			//TODO Enable this once we sort out how to get and use the correct refresh. If we try to run .refresh on a model, we might get the one here or in metadata properties or base device!
			refresh: function () {
				this._detailPromise = this.fetch().then(this.fetchMetaData);
				return this._detailPromise;
			},

			getMetaDataPromise: function () {
				return this._metaDataFetchPromise || this.fetchMetaData();
			}
		};

		_.extend(defaults, buildDefaults(options));
		this.setDefaults(defaults);

		this.before("initialize", function () {
			_.bindAll(this, "fetchMetaData", "refresh", "_persistMetaData", "_makeMetaDataProp");

			this._metaDataProperties = _.union(this._metaDataProperties || [], options.metaDataProperties);
		});
		
		this.before("destroy", function () {
			this._metaDataFetchPromise = null;
		});
	};

	return MetaDataMixin;
});
