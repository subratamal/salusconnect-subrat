"use strict";

define([], function () {

	var CookieManager = {};

	/**
	 * private cookie manager helper. Create a cookie
	 * based on: http://www.quirksmode.org/js/cookies.html
	 * @param name cookie name
	 * @param value cookie value
	 * @param days cookie expiration time
	 */
	CookieManager.createCookie = function (name, value, days) {
		var expires;

		if (days) {
			var date = new Date();

			date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
			expires = "; expires=" + date.toUTCString();
		} else {
			expires = "";
		}

		document.cookie = name + "=" + value + expires + "; path=/";
	};

	/**
	 * private cookie manager helper. Create a cookie
	 * based on: http://www.quirksmode.org/js/cookies.html
	 * @param name name of the cookie
	 * @returns {*} cookie value
	 */
	CookieManager.readCookie = function (name) {
		var nameEQ = name + "=",
			ca = document.cookie.split(';');

		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];

			while (c.charAt(0) === ' ') {
				c = c.substring(1, c.length);
			}

			if (c.indexOf(nameEQ) === 0) {
				return c.substring(nameEQ.length, c.length);
			}
		}

		return null;
	};

	/**
	 * private cookie manager helper. Create a cookie
	 * based on: http://www.quirksmode.org/js/cookies.html
	 * @param name name of the cookie to remove
	 */
	CookieManager.eraseCookie = function (name) {
		CookieManager.createCookie(name, "", -1);
	};

	return CookieManager;
});