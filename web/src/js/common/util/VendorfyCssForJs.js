"use strict";

/**
 * Provides helper methods to add vendor prefixes to a css property or value on an element
 */
define([
	"app",
	"underscore",
	"jquery"
], function (App, _/*, $*/) {

	var VendorfyCssForJs = {},
		vendorPrefixes = ["-ms-", "-moz-", "-o-", "-webkit-"];

	/**
	 * Adds the vendor prefixes to the front of a passed in string
	 * @private
	 */
	VendorfyCssForJs._formatter = function (inputString, prefixesToIgnore) {
		var returnArray = [];

		_.each(vendorPrefixes, function (prefix) {
			if(_.contains(prefixesToIgnore, prefix)) {
				returnArray.push(prefix + inputString);
			}
		});

		// Set the final entry to the non-vendor prefixed property and value
		returnArray.push(inputString);

		return returnArray;
	};

	/**
	 * Takes a single css property and value and formats the property with vendor prefixes then applies to the an element
	 * @param $targetElement the target element to apply the $.css() to
	 * @param cssProp a single css property (e.g. transform)
	 * @param cssValue the css value (e.g. translate3d(0, 0, 0))
	 *
	 * Sets the style on the element like so:
	 * {
	 * 		-ms-transform: translate3d(0, 0, 0),
	 * 		-moz-transform: translate3d(0, 0, 0),
	 *		-o-transform: translate3d(0, 0, 0),
	 * 		-webkit-transform: translate3d(0, 0, 0),
	 * 		transform: translate3d(0, 0, 0)
	 *	}
	 */
	VendorfyCssForJs.formatProperty = function ($targetElement, cssProp, cssValue) {
		var cssPropAndValueAsObject = {},
			propertiesArray = this._formatter(cssProp);

		_.each(propertiesArray, function (prefixedProperty) {
			cssPropAndValueAsObject[prefixedProperty] = cssValue;
		});

		$targetElement.css(cssPropAndValueAsObject);
	};

	/**
	 * Takes a single css property and value and formats the value with vendor prefixes then applies to the an element
	 * Since prefixing values causes duplication of the property name, we apply the style via the $.attr() method,
	 * rather than $.css() method. The $.css() method won't allow us to set multiple css values for the same property.
	 * @param $targetElement the target element to apply the $.attr('style', '') to
	 * @param cssProp a single css property (e.g. transform)
	 * @param cssValue the css value (e.g. translate3d(0, 0, 0))
	 *
	 * Sets the style on the element like so:
	 * {
	 *		background: -ms-linear-gradient( ... );
	 *		background: -moz-linear-gradient( ... );
	 *		background: -o-linear-gradient( ... );
	 *		background: -webkit-linear-gradient( ... );
	 *		background: linear-gradient( ... );
	 *	}
	 */
	VendorfyCssForJs.formatValue = function ($targetElement, cssProp, cssValue) {
		var cssPropsAndValuesString = "",
			valuesArray = this._formatter(cssValue);

		_.each(valuesArray, function (prefixedValue) {
			cssPropsAndValuesString += (cssProp + ": " + prefixedValue + ";");
		});

		$targetElement.attr('style', cssPropsAndValuesString);
	};


	/**
	 * This helper will create and apply the styling to a div that stacks background image and gradient using vendor prefixes
	 * Since this has repeating css property names "background", we apply the style via the $.attr() method,
	 * rather than $.css() method. The $.css() method won't allow us to set multiple css values for the same property like this:
	 * {
	 * 		background: center 50%/48% 48% no-repeat url('/images/icons/dashboard/icon_temp_idle.svg'), -ms-linear-gradient(145deg, #7cc110 0%, #58a708 100%);
	 * 		background: center 50%/48% 48% no-repeat url('/images/icons/dashboard/icon_temp_idle.svg'), -moz-linear-gradient(145deg, #7cc110 0%, #58a708 100%);
	 * 		background: center 50%/48% 48% no-repeat url('/images/icons/dashboard/icon_temp_idle.svg'), -o-linear-gradient(145deg, #7cc110 0%, #58a708 100%);
	 * 		background: center 50%/48% 48% no-repeat url('/images/icons/dashboard/icon_temp_idle.svg'), -webkit-linear-gradient(145deg, #7cc110 0%, #58a708 100%);
	 * 		background: center 50%/48% 48% no-repeat url('/images/icons/dashboard/icon_temp_idle.svg'), linear-gradient(145deg, #7cc110 0%, #58a708 100%);
	 * 	}
	 *
	 * @param $targetElement the target element to apply the $.css or $.attr('style', '') to
	 * @param imgPosition string of css position, e.g. "center 50%/48% 48%"
	 * @param imgRepeatType string of css repeat type, e.g no-repeat
	 * @param imgUrl string path to img url, e.g. "/images/icons/dashboard/icon_groups.svg"
	 * @param gradAngle string of gradient angle with "deg" unit type after the value, e.g. "165deg"
	 * @param gradStartColor string of a normal css hex color code, e.g. "#7cc110"
	 * @param gradStartPosition string of the starting point of the gradient in percentage, e.g. "0%"
	 * @param gradEndColor string of a normal css hex color code, e.g. "#7cc110"
	 * @param gradEndPosition string of the ending point of the gradient in percentage, e.g. "100%"
	 * @param defaultImgUrl substitute url in case of imgUrl failure
	 */
	VendorfyCssForJs.formatBgGradientWithImage = function ($targetElement, imgPosition, imgRepeatType, imgUrl, gradAngle, gradStartColor,
														   gradStartPosition, gradEndColor, gradEndPosition, defaultImgUrl) {
		if (!imgUrl) {
			imgUrl = defaultImgUrl;
			App.warn("VendorfyCSSForJS.formatBgGradientWithImage: Image url is undefined.");
		}

		var cssPropsAndValuesString = "",
			linearGradientString = "linear-gradient(" + gradAngle + ", " + gradStartColor + " " + gradStartPosition + ", " + gradEndColor + " " + gradEndPosition + ")",
			backgroundImageString = imgPosition + " " + imgRepeatType + " url('" + imgUrl + "')",
			valuesArray = this._formatter(linearGradientString, ["-ms-"]);

		_.each(valuesArray, function (prefixedValue) {
			cssPropsAndValuesString += ("background: " + backgroundImageString + ", " + prefixedValue + ";");
		});

		$targetElement.attr('style', cssPropsAndValuesString);
	};

	return VendorfyCssForJs;
});