"use strict";

/**
 * The salus connector is for talking to TBD salus services. The intent is that this is a singleton for the app.
 * TODO:
 *  - should salus connector broker ayla connector calls, or just expose the ayla connector?
 */
define([
	"jquery",
	"backbone",
	"bluebird"
], function ($, B, P) {

	var PromiseHelper = {};

	/**
	 * we have a pattern where we fetch data from ayla that is a user-specific singleton (user, and device list
	 * at least), this wrapper allows us to fetch that with a promise so that the consumer does not need to
	 * implement special code to check loaded state.
	 * @param value the object that currently exists
	 * @param JsClass class that "value" should be. this will be constructed as new jsClass(data, {asPromise:true}); Must extend AylaBacked
	 * @param data the data to construct the new class with if it is not present
	 * @returns {*}
	 */
	PromiseHelper.getModelAsPromise = function (value, JsClass, data) {
		var promise;

		if (!value) {	// this is the not loaded or requested case
			value = new JsClass(data);
			promise = value.getAsPromise();
		} else if (!value.isLoaded()) { // this is the requested already but not loaded case
			promise = value.getReturnValue({asPromise: true});
		} else { // this is the requested and loaded case
			promise = P.resolve(value);
		}

		return {promise: promise, model: value};
	};

	return PromiseHelper;

});
