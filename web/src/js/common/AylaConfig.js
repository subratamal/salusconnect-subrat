"use strict";

define(["app"], function (App) {

	/**
	 * TODO there is a known issue where certain adblock browser extensions
	 * will block calls to ads-dev if they detect ".com/ads".
	 * This will affect dev and prod urls
	 */


	/** note: there seems to be an issue with android and the ec2 url's selfsigned ssl cert on some of the calls.
	 *  for now keep dont move over the salus-web-services.appspot.com calls to ec2 until they have a cert
	 *  affected call seems to be weather.
	 **/
	var AylaConfig = {
		appId: "",
		appSecret: "",	///I hate to put this here
		stdTimeoutMs: 5000, // timeout after 5 seconds
		endpoints: {
			login: "https://eu.salusconnect.io/users/sign_in.json",
			logout: "https://eu.salusconnect.io/users/sign_out.json",
			refreshToken: "https://eu.salusconnect.io/users/refresh_token.json",
			device: {
				list: "https://eu.salusconnect.io/apiv1/devices.json",
				add: "https://eu.salusconnect.io/apiv1/devices.json", // param {device: {dsn: dsn}}
				register: "https://eu.salusconnect.io/apiv1/devices/register.json?regtype=Node&dsn=<%=dsn%>",  // this should be used as a GET and only for the gateway // params are: ip, dsn, regtype {“SameLAN”, “ButtonPush”, “APMode”, “Display”, “Dsn”, “None”(OEM),"Node”(For Gateway)}
				registerButtonPush: "https://eu.salusconnect.io/apiv1/devices/register.json?regtype=Button-Push", //Temporary: point to ayla directly as they need our public client ip to do registration.
				nodesAndProperties: "https://eu.salusconnect.io/apiv1/devices/<%=id%>/nodes.json",
				listByNode: "https://eu.salusconnect.io/apiv1/devices/<%=id%>/registered_nodes.json",
				detail: "https://eu.salusconnect.io/apiv1/devices/<%=id%>.json",
				save: "https://eu.salusconnect.io/apiv1/devices/<%=id%>.json",
				remove: "https://eu.salusconnect.io/apiv1/devices/<%=id%>.json", // delete same as details
				factory_reset: "https://eu.salusconnect.io/apiv1/devices/<%=id%>/cmds/factory_reset.json", // used as put
				property: {
					list: "https://eu.salusconnect.io/apiv1/devices/<%=id%>/properties.json",
					fetch: "https://eu.salusconnect.io/apiv1/properties/<%=id%>.json",
					setDataPoint: "https://eu.salusconnect.io/apiv1/properties/<%=key%>/datapoints.json",
					getDataPointTimeRange: "https://eu.salusconnect.io/apiv1/properties/<%=key%>/datapoints.json",
					getAggregate: "https://eu.salusconnect.io/data/v1/average?dsn=<%=dsn%>&key=<%=prop%>&earliest_time=<%=startDate%>&latest_time=<%=endDate%>&timespan=<%=timespan%>&fmt=json",
					aggregateResults: "https://eu.salusconnect.io/data/results?ref=<%=ref%>"
				},
				address: "https://eu.salusconnect.io/apiv1/devices/<%=id%>/addr.json",
				weather: "https://eu.salusconnect.io/weather?dsn=<%=dsn%>",
				metaData: {
					"fetch": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data/metaData.json",
					"save": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data/metaData.json", // same as fetch,
					"remove": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data/metaData.json",
					"create": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data.json"
				},
				metaDataApi: {
					"fetch": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data/<%=propKey%>.json",
					"fetchAll": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data.json",
					"save": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data/<%=propKey%>.json", // same as fetch,
					"delete": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data/<%=propKey%>.json", // same as fetch,
					"create": "https://eu.salusconnect.io/apiv1/dsns/<%=dsn%>/data.json"
				},
				linkedThermostats: {
					"create": "https://eu.salusconnect.io/linkedDevices/dsns/<%=dsn%>",
					"fetch": "https://eu.salusconnect.io/linkedDevices/dsns/<%=dsn%>?key=<%=key%>",
					"delete": "https://eu.salusconnect.io/linkedDevices/dsns/<%=dsn%>?key=<%=key%>",
					"list": "https://eu.salusconnect.io/linkedDevices/dsns/<%=dsn%>"
				},
				schedules: {
					"list": "https://eu.salusconnect.io/apiv1/devices/<%=deviceId%>/schedules.json",
					"create": "https://eu.salusconnect.io/apiv1/devices/<%=device_id%>/schedules.json",
					"update": "https://eu.salusconnect.io/apiv1/devices/<%=device_id%>/schedules/<%=key%>.json",
					"fetch": "https://eu.salusconnect.io/apiv1/schedules/<%=key%>.json",
					"delete": "https://eu.salusconnect.io/apiv1/schedules/<%=key%>.json",
					"actions": "https://eu.salusconnect.io/apiv1/schedules/<%=schedule_id%>/schedule_actions.json",
					"putActions": "https://eu.salusconnect.io/apiv1/schedule_actions/<%=key%>.json"
				},
				timeZones: {
					"fetch": "https://eu.salusconnect.io/apiv1/devices/<%= key %>/time_zones.json",
                    "detail": "https://eu.salusconnect.io/apiv1/time_zones.json?tz_id=<%=key%>"
				}
			},
			rule: {
				"create": "https://eu.salusconnect.io/rules/dsns/<%=dsn%>",
				"update": "https://eu.salusconnect.io/rules/dsns/<%=dsn%>",
				"fetch": "https://eu.salusconnect.io/rules/dsns/<%=dsn%>?key=<%=key%>",
				"list": "https://eu.salusconnect.io/rules/dsns/<%=dsn%>",
				"delete": "https://eu.salusconnect.io/rules/dsns/<%=dsn%>?key=<%=key%>"
			},
			ruleGroups: {
				"create": "https://eu.salusconnect.io/ruleGroups/dsns/<%=dsn%>",
				"list": "https://eu.salusconnect.io/ruleGroups/dsns/<%=dsn%>", //same as create
				"fetch": "https://eu.salusconnect.io/ruleGroups/dsns/<%=dsn%>?key=<%=key%>",
				"deleteAll": "https://eu.salusconnect.io/ruleGroups/dsns/<%=dsn%>", //same as create
				"delete": "https://eu.salusconnect.io/ruleGroups/dsns/<%=dsn%>?key=<%=key%>", //same as fetch
				"update": "https://eu.salusconnect.io/ruleGroups/dsns/<%=dsn%>" //same as create
			},
			aylaTrigger: {
				fetch: "https://eu.salusconnect.io/apiv1/properties/<%=property_key%>/triggers.json",
				create: "https://eu.salusconnect.io/apiv1/properties/<%=property_key%>/triggers.json",
				update: "https://eu.salusconnect.io/apiv1/triggers/<%=key%>.json",
				delete: "https://eu.salusconnect.io/apiv1/triggers/<%=key%>.json",
				fetchTriggerApps: "https://eu.salusconnect.io/apiv1/triggers/<%=key%>/trigger_apps.json",
				triggerAppDelete: "https://eu.salusconnect.io/apiv1/trigger_apps/<%=key%>.json"
			},
			userProfile: {
				fetch: "https://eu.salusconnect.io/users/get_user_profile.json",
				save: "https://eu.salusconnect.io/users.json<%= query_string_params %>",
				delete: "https://eu.salusconnect.io/users.json",
				changePassword: "https://eu.salusconnect.io/users.json",
                resetPassword: "https://eu.salusconnect.io/users/password.json",
				dataCollectionOff: {
					"fetch": "https://eu.salusconnect.io/api/v1/users/data/dataCollectionOff.json",
					"save": "https://eu.salusconnect.io/api/v1/users/data/dataCollectionOff.json", //same as fetch,
					"create": "https://eu.salusconnect.io/api/v1/users/data.json"
				},
				metaData: {
					"fetch": "https://eu.salusconnect.io/api/v1/users/data/metaData.json",
					"save": "https://eu.salusconnect.io/api/v1/users/data/metaData.json", //same as fetch
					"remove": "https://eu.salusconnect.io/api/v1/users/data/metaData.json", // same as fetch
					"create": "https://eu.salusconnect.io/api/v1/users/data.json"
				},
				tileOrder: {
					"fetch": "https://eu.salusconnect.io/api/v1/users/data/tileOrder.json",
					"save": "https://eu.salusconnect.io/api/v1/users/data/tileOrder.json", // same as fetch,
					"delete": "https://eu.salusconnect.io/api/v1/users/data/tileOrder.json", // same as fetch,
					"create": "https://eu.salusconnect.io/api/v1/users/data.json"
				}
			},
			auth: {
				resendConfirmationToken: "https://eu.salusconnect.io/users/confirmation.json",
				confirmUserToken: "https://eu.salusconnect.io/users/confirmation.json",
				mgPassword: "https://eu.salusconnect.io/users/password.json<%= query_string_params %>"
			},
			groups: {
				list: "https://eu.salusconnect.io/apiv1/groups.json",
				create: "https://eu.salusconnect.io/apiv1/groups.json", //same as list
				remove: "https://eu.salusconnect.io/apiv1/groups/<%=key%>.json",
				load: "https://eu.salusconnect.io/apiv1/groups/<%=id%>.json", //same as remove
				put: "https://eu.salusconnect.io/apiv1/groups/<%=key%>.json", //same as load
				addDevice: "https://eu.salusconnect.io/apiv1/groups/<%=key%>/devices.json",
				removeDevice: "https://eu.salusconnect.io/apiv1/groups/<%=key%>/devices/<%=device_id%>.json",
				getDatapoints: "https://eu.salusconnect.io/apiv1/groups/<%=key%>/datapoints.json<%= query_string_params %>"
			},
			share: {
				createProxy: "https://eu.salusconnect.io/proxyusers.json",
				list: "https://eu.salusconnect.io/api/v1/users/shares.json",
				create: "https://eu.salusconnect.io/api/v1/users/shares.json",
				read: "https://eu.salusconnect.io/api/v1/users/shares/<%=id%>.json",
				update: "https://eu.salusconnect.io/api/v1/users/shares/<%=id%>.json",
				delete: "https://eu.salusconnect.io/api/v1/users/shares/<%=id%>.json",
				batchShare: "https://eu.salusconnect.io/batchShare.json"
			},
			photo: {
				create: "https://eu.salusconnect.io/photo",
				update: "https://eu.salusconnect.io/photo",
				remove: "https://eu.salusconnect.io/photo"
			},
			log: {
				create: "https://eu.salusconnect.io/services/collector/event"
			}
		},
		/**
		 * Builds a localized version of an Ayla URL so that the Nginx config can proxy pass to the correct endpoint
		 * @param aylaUrlPart
		 * @returns {*}
		 */
		buildLocalURLForAyla: function (aylaUrlPart) {

			if (aylaUrlPart.toLowerCase().indexOf("http") === 0) {
				return aylaUrlPart;
			}

			var hostName = window.location.hostname,
				host = window.location.origin || (window.location.protocol + "//" + window.location.host); //IE fixes

			if (host === "file://" || hostName.indexOf("localhost") >= 0 || hostName.indexOf("210.75.16.197") >= 0) {
				host = "https:/"; //TODO: config drive this per environment.

				//TODO QUICK FIX
				aylaUrlPart = aylaUrlPart.replace("salusconnect.io/", App.getIsEU() ? "eu.salusconnect.io/" : "us.salusconnect.io/");
			}

			// special case for staging to hit itself
			if (hostName.indexOf("salusconnect") >= 0 && aylaUrlPart.indexOf("salusconnect.io/") === 0) {
				host = "";
				aylaUrlPart = aylaUrlPart.replace("salusconnect.io/", "");
			}

            if (aylaUrlPart.indexOf("?") >= 0) {
                aylaUrlPart = aylaUrlPart + "&";
            } else {
                 aylaUrlPart = aylaUrlPart + "?";
            }
            aylaUrlPart = aylaUrlPart + "timestamp=" + new Date().getTime();

			return host + "/" + aylaUrlPart;
		},
		/**
		 * Jasmine tests call this method to determine what protocol to expect in an Ayla ajax URL.
		 * In order to get proxy passing to work, we needed to move the setting of "https://" out of the javascript and
		 * into the dev and int nginx configs. During a proxy pass, the server will prepend "https://"
		 * onto any Ayla URL. This means that any Ayla calls made on desktop should hit "http".
		 * @returns {"http", "https"}
		 *
		 */
		getURLProtocolForAylaCalls: function () {
			// TODO add logic here to determine WHEN to set this to http or https (i.e. for testing on mobile, since those calls will be made directly to Ayla)
			return "http";
		}
	};

	return AylaConfig;
});
