#!/usr/bin/python

import os, sys

from subprocess import Popen
from datetime import datetime

deploymentCommands = [
	[
		"Installing node modules",
		"npm install"
	],
	[
		"Build Dev",
		"./node_modules/gulp/bin/gulp.js --buildNumber=" + os.getenv('BUILD', 'Development') \
			+ " --versionNumber=" + os.getenv('VERSIONNUMBER', '0.0.1') \
			+ " --releaseDate=" + os.getenv('RELEASEDATE', '') \
			+ " --environment=" + os.getenv('ENVIRONMENT', 'local') \
			+ "  buildDev"
	],
	[
		"Build Assets for Deployment",
		"./node_modules/gulp/bin/gulp.js buildForDeployment"
	],
	[
		"Prepare Static Assets", 
		"./node_modules/gulp/bin/gulp.js prepareForDeployment"
	],
	[
		"Copy consumer.js to ios",
		"./node_modules/gulp/bin/gulp.js copyWebFiles"

	],
	[
		"Copy Smashed Version to index for ios",
		"cp cordova/platforms/ios/www/smashedMobile.html cordova/platforms/ios/www/index.html"
	],
	[
		"Copy Smashed Version to index for androiod",
		"cp cordova/www/smashedMobile.html cordova/www/index.html"
	],
	[
		"Hash Files",
		"./tools/hashFiles.py"
	]
]

for cmd in deploymentCommands:
	print "\n" + cmd[0]
	p = Popen(cmd[1], shell=True)
	status = p.wait()

	if status != 0:
		print "Command returned %(status)d: %(command)s" %\
				{'status': status, 'command': cmd[1]}
		sys.exit(1)